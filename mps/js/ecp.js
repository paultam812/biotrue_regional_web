$(function() {

	/* ABOUT/FAQ */
	$('#faq-panel').find('.content').append($('#faq-panel').prev('.panel-content'));
	
	
	
	/* REQUEST SAMPLE */
//	$('#samples iframe body').append($('link').attr('href', 'http://www.soultrust.com/test/biotrue/iframe.css').attr('rel', 'stylesheet').attr('type', 'text/css'));
	
//	var cssLink = document.createElement("link") 
//	cssLink.href = "http://www.soultrust.com/test/biotrue/iframe.css"; 
//	cssLink .rel = "stylesheet"; 
//	cssLink .type = "text/css"; 
//	frames['#request-sample'].document.body.appendChild(cssLink);



	$('#swflink').click(function() {
		var flashvars = false;
		var params = {wmode: "transparent"};
		swfobject.embedSWF("../biotrue.swf", "flashobj", "960", "830", "9.0.0","", flashvars, params);
		$('#contentLayout, #bg_pageSpecific').hide();
		$('#flashcontainer').show();
		return false;
	});

	
	// Set up hover states for main sub navs to accommodate IE lack of hover support
	$('.ecp #nav ul#navlist li li').not(':has(.disableRollover)').hover(
		function() {
			$(this).css('background', function() {
				if ($.browser.msie) return 'url(/img/bullet_green.gif) no-repeat 0em 0.2em'
				else return 'url(/img/bullet_green.gif) no-repeat 0em 0.3em';
			}).find('a').css('color', '#4ab74e');
		},
		
		function() {
			$(this).css('background', 'none').find('a').css('color', '#0093cc');;
		}
	);
	
	$('.ecp a[href=#references]').tooltip();
	
	$(window).load(function() {
		$('#testimonials').panelResize();
	});
});