$(function() {

	$('#faq-panel').panelResize();
	
	$('.question').click(function() {
		var resizingInterval = setInterval("$('#faq-panel').panelResize()", 50);
		$(this).next('.answer').slideDown(500, function(){
			clearInterval(resizingInterval);
			$('#faq-panel').panelResize()
		});
		$('.answer').not($(this).next('.answer')).slideUp(500);
	});

});