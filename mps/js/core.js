$(function(){
	
	// Set up superfish on nav
	$('#nav ul#navlist').superfish({
		onInit: function() {
			//loaded = true;
		},
		onBeforeShow: function() {
			$(this).css({
				position: 'absolute',
				left: 0,
				top: 37,
				zIndex: 1000
			});
		},
		onHide: function() {
			$(this).hide();
			//if (loaded) $('.rollover').remove();
		},
		delay: 100
	});
	
	
	// Highlight the selected top level section in nav
	$('#nav_' + $('body').attr('id') + ' img')
		.addClass('disableRollover')
		.each(function(i) {
			var oversrc = this.src.toString().replace('_off', '_on');
			var ii = new Image();
			ii.src = oversrc;
			this.src = oversrc;
		});
	
	$('.sub #nav_' + $('body').attr('id'))
		.parents('li.level1:first').find('img')
		.addClass('disableRollover')
		.each(function(i) {
			var oversrc = this.src.toString().replace('_off', '_on');
			var ii = new Image();
			ii.src = oversrc;
			this.src = oversrc;
		});
		
	/* Highlight the selected sublevel page in nav and have the parent subnav open on page load
	$('#nav_' + $('body').attr('id') + ' > a').not('a:has(img)')
		.addClass('disableRollover')
		.parents('li:first')
		.addClass('selected')
		.parents('ul:first')
		.show()
		.css({
			visibility: 'visible',
			position: 'absolute',
			top: 37,
			left: 0
		});
	*/
	
	// Set up image rollovers for main nav
	var preLoads = [];
	
	$('ul#navlist img').not('.disableRollover').not('#nav_pro img').each(function(i) {
	
		var oversrc, t, s;
	    oversrc = this.src.toString().replace('_off', '_on');
	    preLoads[i] = new Image();
	    preLoads[i].src = oversrc;
	    
	    $(this).hover(
            function(e){
                t = e.target;
                s = t.src;
                t.src = s.replace('_off', '_on');
            },
            function(e) {
                t = e.target;
                s = t.src;
                t.src = s.replace('_on', '_off');
            }
	    );
	});
	
	
	// Set up FADE IN/OUT rollovers for everything else
	var preLoads2 = [];
	$('.cta img').each(function(ii) {
		// if ($.browser.msie || $.browser.safari) {
			var oversrc, t, s;
		    oversrc = this.src.toString().replace('_off', '_on');
		    preLoads2[ii] = new Image();
		    preLoads2[ii].src = oversrc;
		    
		    $(this).hover(
	            function(e){
	                t = e.target;
	                s = t.src;
	                t.src = s.replace('_off', '_on');
	            },
	            function(e) {
	                t = e.target;
	                s = t.src;
	                t.src = s.replace('_on', '_off');
	            }
		    );
		// } else {
		// 
		// 
		// 	
		// 	var dis = $(this);
		// 	var cssObj = {
		// 		position: 'absolute',
		// 		left: function() {return dis.position().left;}, 
		// 		top: function() {return dis.position().top;},
		// 		width: dis.width(),
		// 		height: dis.height()
		// 	}
		// 	
		// 	dis.after($('<img src="/img/trans.gif" />')
		// 		.css(cssObj)
		// 		.hover(
		// 			function() {
		// 				if (dis.nextAll('.rollover').size() > 0) {
		// 					dis.next('.rollover').remove();
		// 				} 
		// 				
		// 				dis.after(
		// 					$('<img class="rollover">')
		// 						.attr('src', dis.attr('src').replace('_off', '_on'))
		// 						.css(cssObj).show()
		// 						.stop().stop().fadeIn(400)
		// 				);
		// 				
		// 			},
		// 			function() {
		// 				$(this).prev('.rollover').stop().stop().fadeOut('slow', function() {$(this).remove()});
		// 			}
		// 		)
		// 	);
		// }
	
	});

	
	/* Special case for Pro/Consumer button because it fades and it's a gif, not a png */
	$('#nav_pro img, #nav_consumer img').not('.disableRollover').each(function() {
		
			var dis = $(this);
			var cssObj = {
				position: 'absolute',
				left: function() {return dis.position().left;}, 
				top: function() {return dis.position().top;},
				width: dis.css('width'),
				height: dis.css('height')
			}
			
			dis.after($('<img src="img/trans.gif" />')
				.css(cssObj)
				.hover(
					function() {
						$('#pro_mouseover_text').fadeIn('slow');
						if (dis.nextAll('.rollover').size() > 0) {
							dis.next('.rollover').remove();
						} 
						
						dis.after(
							$('<img class="rollover">')
								.attr('src', dis.attr('src').replace('_off', '_on'))
								.css(cssObj).show()
								.stop().stop().fadeIn(400)
						);
						
					},
					function() {
						$('#pro_mouseover_text').fadeOut('slow');
						$(this).prev('.rollover').stop().stop().fadeOut('slow', function() {$(this).remove()});
					}
				)
			);
	});
	
	var recalculateFooterPosition = function() {
		var contentBottomYPosition = $('#contentBottomMark').offset().top;
		
		if ((contentBottomYPosition > $(window).height()-40) || ($('body').attr("id") == 'home')) {
			$('#footer').css({
				position: 'relative',
				visibility: 'visible'
			});
			$(window).css({overflow: 'auto'});
		} else {
			$('#footer').css({
				position: 'absolute',
				bottom: '0px',
				visibility: 'visible'
			});
			$(window).css({overflow: 'hidden'});
		}
	};
	
	var widthConstraint = function() {
		if ($('html').width() <= 960) {
			$('#bg_pageSpecific, #nav, #footer').width(960);
		} else {
			$('#bg_pageSpecific, #nav, #footer').css('width', '100%');
		}
	};
	
	$(window).load(function() {
		recalculateFooterPosition();
		widthConstraint();
	});
	
	
	var horizontalScroll = function() {
		if ($('html').width() < 960) {
			$('html').css({overflow: 'auto'});
			$('#nav').css('left', -54);
		} else {
			$('#nav').css('left', null);
			$('#nav').css('right', 15);
		}
	};
	
	
	// Set up hover states for main sub navs to accommodate IE lack of hover support
	$('#nav ul#navlist li li').not(':has(.disableRollover)').hover(
		function() {
			$(this).css('background', function() {
				if ($.browser.msie) return 'url(/img/bullet_green.gif) no-repeat 0em 0.5em'
				else return 'url(/img/bullet_green.gif) no-repeat 0em 0.3em';
			}).find('a').css('color', '#4ab74e');
		},
		
		function() {
			$(this).css('background', 'none').find('a').css('color', '#0093cc');;
		}
	);
	
	
	// Set up tooltips
	$('.moreInfo').click(function() {
		var trigger = $(this);
		$('.tooltip').css({
			position: 'absolute',
			top: function() {return trigger.offset().top+10},
			left: function() {return trigger.offset().left+10}
		}).fadeIn('slow');
		return false;
	});
	
	$('.tooltip .tooltipClose').click(function() {
		$(this).parents('.tooltip').fadeOut('slow');
	});
	
	var closeTooltip = function() {
		if ($('.tooltip:visible').size()) {
			$('.tooltip').fadeOut('fast');
		}
	};
	
	
	
	// On window resize...
	$(window).bind('resize', function() {
    	recalculateFooterPosition();
    	//horizontalScroll();
    	widthConstraint();	
    	closeTooltip();
	});
	
	
	// Tooltip 	
	$.fn.tooltip = function() {
		return this.each(function() {
			$(this).click(function() {
				var trigger = $(this);
				var anchor = $(this).attr('href').substring($(this).attr('href').indexOf('#')+1);
				$('.tooltip[id="tooltip_'+ anchor +'"]').css({
					position: 'absolute',
					top: function() {return trigger.offset().top+10},
					left: function() {return trigger.offset().left + trigger.width()}
				}).fadeIn('slow');
				return false;
			});
		});
	};
	
	// Function for resizing png shadows for all panels using template
	$.fn.panelResize = function() {
		return this.each(function() {
			var tableHeight = $(this).find('.content').height() + 96;		
			var tableWidth = $(this).width();
			$(this).find('.left .pngBG, .right .pngBG').height(tableHeight - 53);
			$(this).find('.bottom .pngBG').width(tableWidth - 32);
			$(this).find('.top .pngBG').width(tableWidth - 32);
		});
	};
	
	$('.panel').panelResize();

});