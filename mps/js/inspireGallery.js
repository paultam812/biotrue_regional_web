$(function() {
	
	var loadingInProgress = true;
	$('#debug').html(loadingInProgress + '');
	var currentImg = 0;
	var preLoads = [];
	var imgs = document.images;

	inspireJsonObj = {"gallery":
		[{
			"inspiration": "img/inspiregallery/fish.jpg",
			"result": "img/inspiregallery/car.jpg",
			"title": "Aerodynamic cars inspired by the boxfish",
			"description": "The boxfish has a large, yet streamlined, body that allows it to swim very fast. This design inspired a new car that creates incredibly low levels of drag."
		},
		{
			"inspiration": "img/inspiregallery/butterfly.jpg",
			"result": "img/inspiregallery/smartphone.jpg",
			"title": "Energy-efficient displays inspired by the butterfly",
			"description": "The butterfly's wings have transparent scales, which diffract and scatter light, rather than absorbing and reflecting it. This design has inspired new display technologies that use less energy."
		}]
	};


	var loadInspiration = function(slideNum) {
		var img = new Image();
		$(img).load(function() {
			$('#inspirationImg').fadeIn('slow');
			$(this).hide();
			$('#inspirationImg').append(this);
			
			
			$(this).fadeIn('slow', function() {
				$('#arrow').fadeIn('slow', function() {
					loadResult(slideNum);	
				})
			});
			
		})
		.attr('src', inspireJsonObj.gallery[slideNum].inspiration);
	};
	
	var loadResult = function(slideNum) {
		var img = new Image();
		$(img).load(function() {
			$('#resultImg').fadeIn('slow');
			$(this).hide();
			$('#resultImg').append(this);
			$(this).fadeIn('slow', function() {
				loadDescription(slideNum);	
			});
		})
		.attr('src', inspireJsonObj.gallery[slideNum].result);
	};

	var loadDescription = function(slideNum) {
		$('#description').fadeIn('slow', function() {fixPanelsForIE(); loadingInProgress = false; $('#debug').html(loadingInProgress + '');});
		$('#description .contentTD .content')
			.append($('<h1>').append(inspireJsonObj.gallery[slideNum].title))
			.append($('<p>').append(inspireJsonObj.gallery[slideNum].description));
	};

	var nextSlide = function() {
		if (loadingInProgress) {return;}
			else {loadingInProgress = true;}
		$('#inspirationImg').fadeOut('slow', function() {
			$('#inspirationImg img').remove();
			$('#arrow').fadeOut('slow', function() {
				$('#resultImg').fadeOut('slow', function() {
					$('#resultImg img').remove();
					if (currentImg == inspireJsonObj.gallery.length-1) {
						currentImg = 0;
						loadInspiration(0);
					} else {
						loadInspiration(++currentImg);
					}
					$('#description').fadeOut('slow', function() {
						$('#description .contentTD .content').empty();
					});
				})
			})
		});
	};
	
	var prevSlide = function() {
		if (loadingInProgress) {return;}
			else {loadingInProgress = true;}
		$('#inspirationImg').fadeOut('slow', function() {
			$('#inspirationImg img').remove();
			$('#arrow').fadeOut('slow', function() {
				$('#resultImg').fadeOut('slow', function() {
					$('#resultImg img').remove();
					if (currentImg == 0) {
						var lastSlideNum = inspireJsonObj.gallery.length-1;
						loadInspiration(lastSlideNum);
						currentImg = lastSlideNum;
					} else {
						loadInspiration(--currentImg);
					}
					$('#description').fadeOut('slow', function() {
						$('#description .contentTD .content').empty();
					});
				})
			})
		});
	};
	
	var init = function() {
		$('#inspirationImg, #arrow-ie, #arrow-else, #resultImg, #description').hide();
		
		if ($.browser.msie) {
			$('#arrow-ie').attr('id', 'arrow');
		} else {
			$('#arrow-else').attr('id', 'arrow');
		}

		loadInspiration(currentImg);
		$('#nextBtn').click(function() {
			nextSlide();
			return false;
		});
		$('#prevBtn').click(function() {
			prevSlide();
			return false;
		});
		
	};
	
	var fixPanelsForIE = function() {
		$('.panel').each(function() {
			var tableHeight = $(this).height();
			var tableWidth = $(this).width();
			$(this).find('.left .pngBG, .right .pngBG').height(tableHeight - 53);
			$(this).find('.bottom .pngBG').width(tableWidth - 32);
			$(this).find('.top .pngBG').width(tableWidth - 32);
		});
	};

	init();

});