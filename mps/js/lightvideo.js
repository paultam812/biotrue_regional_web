if (!Function.prototype.bind) {
	Function.prototype.bind = function(){
		var method = this, args = Array.prototype.slice.call(arguments), src = args.shift();
	  return function() {
			return method.apply(src, args.concat(Array.prototype.slice.call(arguments)));
		};
	};	
}

function lightvideo(video){
	this.player_class = "lightvideo_player";
	this.overlay_id = "lightvideo_overlay";
	this.positioning_class = "lightvideo_positioning";
	this.initialize(video);
}

lightvideo.prototype = {
	
	initialize : function(video){
		this.video = $(video);
		this.video_player = $(this.video.attr("href"));
		
		// we need the player markup - return if null
		if (!this.video_player.length)
			return;
		
		this.body = $(document.body);
		this.video_overlay = this.createOverlay();
		this.video_positioning = this.createVideoPositioning();
		this.video_positioning.append(this.video_player);
		this.video.click(this.linkClickHandler.bind(this));
	},
	
	linkClickHandler : function(){
		this.displayVideoElements();
	},
	
	createVideoPositioning : function(){
		var pre_existing_positioning = $("." + this.positioning_class);
		if (pre_existing_positioning.length)
			return pre_existing_positioning;
		this.body.append(
			$("<div class='" + this.positioning_class + "'></div>").append(
				$("<img src='/img/tooltipClose.gif' class='closeOverlay' alt='' />").click(this.hideVideoElements.bind(this))));
		return $("." + this.positioning_class).last();
	},
	
	createOverlay : function(){
		var pre_existing_overlay = $("#" + this.overlay_id);
		if (pre_existing_overlay.length)
			return pre_existing_overlay;
		this.body.append("<div id='" + this.overlay_id + "'></div>");
		return $("#" + this.overlay_id);
	},
	
	displayVideoElements : function(){
		this.video_overlay.css("height", this.getTotalBrowserHeight());
		this.video_overlay.fadeIn(400, function(){
			if ($.browser.msie)
				this.video_overlay.css({ "opacity" : 0.5 });
			this.video_positioning.
				css("left", (this.body.width() / 2) - 250).
				css("top", this.calculateDistanceFromTop());
			this.video_positioning.fadeIn(400, function(){
				this.video_player.show();
				this.setUpVideoPlayer();
				if (this.videoJSPlayer)
					this.playVideo();
			}.bind(this));
		}.bind(this));
		this.video_overlay.click(this.hideVideoElements.bind(this));
	},
	
	hideVideoElements : function(){
		this.stopPlayingVideo();
		this.video_positioning.hide();
		this.video_overlay.fadeOut(400).unbind("click");
	},
	
	setUpVideoPlayer : function(){
		if (this.videoJSPlayer)
			return;
		var video = $("video", this.video_player)[0];
		if (VideoJS.supportsVideo()) {
			this.videoJSPlayer = new VideoJS(video, 0);
		}
	},
	
	playVideo : function(){
		if (this.videoJSPlayer)
			this.videoJSPlayer.video.play();
	},
	
	stopPlayingVideo : function(){
		if (this.videoJSPlayer)
			this.videoJSPlayer.video.pause();
	},
	
	getTotalBrowserHeight : (function(){
		// hacky, but keeps overlay looking good in all browsers
		if ($.browser.msie) {
			return function(){
				return document.body.scrollHeight;
			};
		} else if ($.browser.webkit) {
			return function(){
				return document.height || document.body.offsetHeight;
			};
		} else {
			return function(){
				return window.scrollMaxY + document.body.clientHeight;
			};
		}
	})(),
	
	calculateDistanceFromTop : function(){
		var scrolled = window.pageYOffset;
		if (scrolled === undefined) // IE
			scrolled = document.documentElement.scrollTop || document.body.scrollTop;
		var window_height = window.innerHeight || document.documentElement.clientHeight;
		return (window_height / 2) - 170 + scrolled;
	}
	
};

$(function(){
	
	$(".lightvideo").each(function(i, video){
		new lightvideo(video);
	});
	
});