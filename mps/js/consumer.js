$(function(){
	
	var fixPanelsForIE = function() {
		$('.panel').each(function() {
			var tableHeight = $(this).height();
			var tableWidth = $(this).width();
			$(this).find('.left .pngBG, .right .pngBG').height(tableHeight - 53);
			$(this).find('.bottom .pngBG').width(tableWidth - 32);
			$(this).find('.top .pngBG').width(tableWidth - 32);
		});
	};
	fixPanelsForIE();
	
	
	$('#swflink').click(function() {
		var flashvars = false;
		var params = {wmode: "transparent"};
		swfobject.embedSWF("biotrue.swf", "flashobj", "960", "830", "9.0.0","expressInstall.html", flashvars, params);
		$('#content .padding').hide();
		$('#flashcontainer').show();
		return false;
	});
	
});