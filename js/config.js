// Development Config
let config = {
  apiDomain: "http://localhost:8888/rest", // Development Server	
  // apiDomain: "http://localhost:81/biotrue_web/rest", // Windows Local Server
  pageDomain:"https://test.tarvel.co/biotrue",
  // cmsPageDomain:'http://cms.biotrue.tarvel.co/redeem',
  smsDomain: "https://api.accessyou.com",
};

// Test Server Config
// let config = {
//   apiDomain: "https://test.tarvel.co/biotrue/rest", // Windows Local Server
//   pageDomain:"https://test.tarvel.co/biotrue",
//   // cmsPageDomain:'http://cms.biotrue.tarvel.co/redeem',
//   smsDomain: "https://api.accessyou.com",
// };

// Live Server Config
// let config = {
//   pageDomain:"https://biotrue.tarvel.co",
//   apiDomain: "https://biotrue.tarvel.co/rest", // Live server
//   smsDomain: "https://api.accessyou.com",
// };

