const region = [
	"香港",
	"九龍",
	"新界",
	"澳門"
];

const storeListData = {
    "香港": [
        {
            "company": "博視眼鏡",
            "region": "香港",
            "district": "上環",
            "address": "德輔道中288號易通商業大廈地下A2號舖",
            "telephone": "28516609",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "文華眼鏡",
            "region": "香港",
            "district": "中環",
            "address": "德輔道中48-52 號裕昌大廈1樓101",
            "telephone": "27856533",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "精明眼鏡",
            "region": "香港",
            "district": "中環",
            "address": "皇后大道中33號萬邦行大廈1605室",
            "telephone": "28453061",
            "product1": "0",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "CENTRAL EYE CARE LIMITED      ",
            "region": "香港",
            "district": "中環",
            "address": "皇后大道中50-52號陸佑行902室",
            "telephone": "35216060",
            "product1": "0",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "瑞士寶視光體驗中心-眼科視光中心",
            "region": "香港",
            "district": "中環",
            "address": "威靈頓街52號23樓",
            "telephone": "39710873",
            "product1": "0",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "明星眼鏡",
            "region": "香港",
            "district": "北角",
            "address": "英皇道278-288號英皇柏麗道地下商場61舖",
            "telephone": "25780013",
            "product1": "0",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "新時代眼鏡",
            "region": "香港",
            "district": "北角",
            "address": "英皇道288號柏麗大道地下3號舖",
            "telephone": "25129327",
            "product1": "0",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "睛采視力",
            "region": "香港",
            "district": "北角",
            "address": "和富道80號仁寶閣地下",
            "telephone": "28979203",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "聯邦眼鏡公司",
            "region": "香港",
            "district": "太古城",
            "address": "太古城中心4樓406舖",
            "telephone": "28936889",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "眼光獨到有限公司",
            "region": "香港",
            "district": "小西灣",
            "address": "藍灣廣場UG25號",
            "telephone": "34074166",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "明視眼鏡公司",
            "region": "香港",
            "district": "柴灣",
            "address": "柴灣道345號金源洋樓地下1號舖",
            "telephone": "29750335",
            "product1": "1",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "雅仕眼鏡",
            "region": "香港",
            "district": "柴灣",
            "address": "興華邨商場地下07B舖",
            "telephone": "28890988",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "新翠眼鏡",
            "region": "香港",
            "district": "柴灣",
            "address": "新翠邨新翠商場4樓35號舖",
            "telephone": "28982182",
            "product1": "1",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "VISUAL PLUS",
            "region": "香港",
            "district": "柴灣",
            "address": "杏花邨杏花新城東翼127號舖",
            "telephone": "24206833",
            "product1": "1",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "永光眼鏡公司",
            "region": "香港",
            "district": "灣仔",
            "address": "莊士敦道35-45號利文樓地下1C 號舖",
            "telephone": "25271870",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "式時眼鏡",
            "region": "香港",
            "district": "灣仔",
            "address": "軒尼詩道438號金鵝商業大廈9樓",
            "telephone": "28937879",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "皇冠隱形眼鏡公司",
            "region": "香港",
            "district": "灣仔",
            "address": "軒尼詩道344號地下",
            "telephone": "28922688",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "皇冠隱形眼鏡公司",
            "region": "香港",
            "district": "灣仔",
            "address": "灣仔道地下218B號",
            "telephone": "28910232",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "美樂眼鏡",
            "region": "香港",
            "district": "灣仔",
            "address": "駱克道315號駱基中心4樓C室",
            "telephone": "28274198",
            "product1": "0",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "新誠眼鏡貿易公司",
            "region": "香港",
            "district": "灣仔",
            "address": "灣仔道151-163號新世紀廣場1樓163A舖",
            "telephone": "28384023",
            "product1": "0",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "蔣氏眼鏡",
            "region": "香港",
            "district": "灣仔",
            "address": "莊士敦道8號地下",
            "telephone": "25282286",
            "product1": "0",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "聯邦眼鏡公司",
            "region": "香港",
            "district": "灣仔",
            "address": "軒尼詩道302-308號集成中心G6號地下",
            "telephone": "28954311",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "麗詩眼鏡",
            "region": "香港",
            "district": "灣仔",
            "address": "春園街1-11號春暉大廈地下1號舖 ",
            "telephone": "28341100",
            "product1": "0",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "視覺護理中心",
            "region": "香港",
            "district": "灣仔",
            "address": "軒尼詩道302-8號集成中心712室",
            "telephone": "25302789",
            "product1": "0",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "保健視力",
            "region": "香港",
            "district": "灣仔",
            "address": "軒尼詩道302-308號集成中心商場UG13號舖",
            "telephone": "21210178",
            "product1": "0",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "東明眼鏡",
            "region": "香港",
            "district": "筲箕灣",
            "address": "西灣河成安街18號港島東18地下G06B舖",
            "telephone": "25695528",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "偉明眼鏡",
            "region": "香港",
            "district": "筲箕灣",
            "address": "筲箕灣道360A號天悅筲箕灣廣場地下LG6號",
            "telephone": "25130235",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "博視眼鏡",
            "region": "香港",
            "district": "筲箕灣",
            "address": "東大街141-151號金基商場地下2號舖",
            "telephone": "25609328",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "熊猫明星眼鏡有限公司",
            "region": "香港",
            "district": "筲箕灣",
            "address": "南康街17號天悅筲箕灣廣場1樓1059號舖",
            "telephone": "39568635",
            "product1": "0",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "明視眼鏡公司",
            "region": "香港",
            "district": "筲箕灣",
            "address": "筲箕灣道33-55號麗灣大廈A號舖地下   ",
            "telephone": "21528823",
            "product1": "1",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "天幕眼鏡",
            "region": "香港",
            "district": "西營盤",
            "address": "皇后大道西300號",
            "telephone": "25597098",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "日昌眼鏡",
            "region": "香港",
            "district": "西環",
            "address": "卑路乍街西寶城2樓215號舖",
            "telephone": "25427015",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "盈采眼鏡",
            "region": "香港",
            "district": "西環",
            "address": "石塘咀皇后大道西425L G/F",
            "telephone": "25173805",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "健生眼鏡公司",
            "region": "香港",
            "district": "西環",
            "address": "石塘咀山道2號B地下",
            "telephone": "28197154",
            "product1": "1",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "健生眼鏡公司",
            "region": "香港",
            "district": "西環",
            "address": "吉席街65號嘉安大厦A座地下B室",
            "telephone": "28176997",
            "product1": "1",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "華麗專業眼鏡",
            "region": "香港",
            "district": "西環",
            "address": "和合街6-12號國基大廈地下F號舖",
            "telephone": "28167028",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "新一代眼鏡",
            "region": "香港",
            "district": "金鐘",
            "address": "夏愨道18號海富中心2樓34舖",
            "telephone": "21210295",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "世紀眼鏡香港有限公司",
            "region": "香港",
            "district": "銅鑼灣",
            "address": "邊寧頓街9-11號登龍閣地下D室",
            "telephone": "25774081",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "目廊專業驗眼中心",
            "region": "香港",
            "district": "銅鑼灣",
            "address": "銅鑼灣廣場第一期209室",
            "telephone": "25752382",
            "product1": "1",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "宇宙視力驗配中心",
            "region": "香港",
            "district": "銅鑼灣",
            "address": "渣甸坊3號蓮福商業大廈3樓",
            "telephone": "28829322",
            "product1": "1",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "百利眼鏡公司",
            "region": "香港",
            "district": "銅鑼灣",
            "address": "紀利佐治街1號金百利11樓1101A室",
            "telephone": "28820468",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "香港專業護眼中心",
            "region": "香港",
            "district": "銅鑼灣",
            "address": "怡和街28號恒生銅鑼灣大廈12樓C室",
            "telephone": "39041269",
            "product1": "0",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "康視眼鏡公司",
            "region": "香港",
            "district": "銅鑼灣",
            "address": "渣甸英廣大廈地下7號",
            "telephone": "25766447",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "新時代眼鏡",
            "region": "香港",
            "district": "銅鑼灣",
            "address": "渣甸坊63號E地下",
            "telephone": "28080098",
            "product1": "0",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "義莎眼鏡珠寶有限公司",
            "region": "香港",
            "district": "銅鑼灣",
            "address": "告士打道255-258號信和廣場地下及1樓",
            "telephone": "23661256",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "夢想成真",
            "region": "香港",
            "district": "銅鑼灣",
            "address": "羅素街38號金朝陽廣場1503-6號",
            "telephone": "23882262",
            "product1": "0",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "百德眼鏡中心",
            "region": "香港",
            "district": "銅鑼灣",
            "address": "軒尼詩道458-468 號金聯商業中心802",
            "telephone": "28932940",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "J VISION",
            "region": "香港",
            "district": "銅鑼灣",
            "address": "軒尼詩道555號東角中心13樓1305室",
            "telephone": "29703818",
            "product1": "0",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "眼鏡大師",
            "region": "香港",
            "district": "銅鑼灣",
            "address": "駱克道459-461號L廣場地下",
            "telephone": "28384828",
            "product1": "0",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "明華眼鏡公司",
            "region": "香港",
            "district": "香港仔",
            "address": "成都路香港仔中心第2期1樓33及35號舖",
            "telephone": "25548997",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "創藝眼鏡有限公司",
            "region": "香港",
            "district": "香港仔",
            "address": "湖南街12號地舖",
            "telephone": "25528607",
            "product1": "1",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "雅麗眼鏡",
            "region": "香港",
            "district": "香港仔",
            "address": "香港仔大道223-227號利群商場地下9B號舖",
            "telephone": "25556005",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "寶視眼鏡公司",
            "region": "香港",
            "district": "香港仔",
            "address": "香港仔大道223-227號利群商場1樓25號舖",
            "telephone": "25541678",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "博視專業驗眼中心",
            "region": "香港",
            "district": "鰂魚涌",
            "address": "康怡廣場1樓F18舖",
            "telephone": "31022282",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "EYE.COM",
            "region": "香港",
            "district": "鴨脷洲",
            "address": "海怡廣場（西翼）G29A",
            "telephone": "22179738",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "明視眼鏡公司",
            "region": "香港",
            "district": "鴨脷洲",
            "address": "鴨脷洲西邨利澤樓37號舖 ",
            "telephone": "25547671",
            "product1": "1",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "城市新視力",
            "region": "香港",
            "district": "鴨脷洲",
            "address": "海怡路18A號海怡西商場G08A舖",
            "telephone": "25534433",
            "product1": "0",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "峰景視力",
            "region": "香港",
            "district": "鴨脷洲",
            "address": "利東商埸一期地下108舖",
            "telephone": "28894552",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        }
    ],
    "九龍": [
        {
            "company": "1 + 1 專業眼鏡",
            "region": "九龍",
            "district": "九龍城",
            "address": "獅子石道81號地下",
            "telephone": "23831839",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "千禧眼鏡",
            "region": "九龍",
            "district": "九龍城",
            "address": "賈炳達道128號九龍城廣場L19舖",
            "telephone": "28381099",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "天明眼鏡公司",
            "region": "九龍",
            "district": "九龍城",
            "address": "獅子石道78號地下",
            "telephone": "23824727",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "雅仕眼鏡",
            "region": "九龍",
            "district": "九龍城",
            "address": "福佬村道45號地下",
            "telephone": "23833373",
            "product1": "1",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "悅目專業眼鏡",
            "region": "九龍",
            "district": "九龍城",
            "address": "福佬村道42-44號置珍閣8-9號舖",
            "telephone": "26921238",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "明昌眼鏡公司",
            "region": "九龍",
            "district": "九龍灣",
            "address": "啟業邨商場5號地舖",
            "telephone": "27593863",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "博視專業驗眼中心",
            "region": "九龍",
            "district": "佐敦",
            "address": "佐敦道8號11樓1103室",
            "telephone": "23770248",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "新眼鏡繽紛",
            "region": "九龍",
            "district": "佐敦",
            "address": "彌敦道218號恒豐中心商場1樓1號舖",
            "telephone": "26873083",
            "product1": "1",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "寶貴視力有限公司",
            "region": "九龍",
            "district": "佐敦",
            "address": "彌敦道216-228A號恒豐中心1樓24-29號舖",
            "telephone": "23756698",
            "product1": "0",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "芍昊眼鏡公司",
            "region": "九龍",
            "district": "何文田",
            "address": "梭椏道11A地下 ",
            "telephone": "27613741",
            "product1": "0",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "明昌眼鏡公司",
            "region": "九龍",
            "district": "土瓜灣",
            "address": "馬頭圍道109號地下",
            "telephone": "23657145",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "保視眼鏡中心",
            "region": "九龍",
            "district": "土瓜灣",
            "address": "美景街4號美景樓1期地下",
            "telephone": "22642802",
            "product1": "0",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "峰景視力",
            "region": "九龍",
            "district": "土瓜灣",
            "address": "馬頭角道33號欣榮商場地下33B舖",
            "telephone": "28862099",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "眼鏡2000",
            "region": "九龍",
            "district": "土瓜灣",
            "address": "土瓜灣道80號T地下",
            "telephone": "23622820",
            "product1": "0",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "匯東光學眼鏡公司",
            "region": "九龍",
            "district": "土瓜灣",
            "address": "馬頭圍道209號海悅豪庭廣場地下G02A",
            "telephone": "24120955",
            "product1": "1",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "新珠光電腦眼鏡",
            "region": "九龍",
            "district": "土瓜灣",
            "address": "馬頭涌道58號地下",
            "telephone": "27131966",
            "product1": "0",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "眼光獨到有限公司",
            "region": "九龍",
            "district": "大角咀",
            "address": "港灣豪庭廣場G20號舖",
            "telephone": "31443196",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "康林眼鏡公司",
            "region": "九龍",
            "district": "大角咀",
            "address": "大角咀道38號新九龍廣場61號地下",
            "telephone": "23925722",
            "product1": "1",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "新時代眼鏡",
            "region": "九龍",
            "district": "奧海城",
            "address": "海暉道11號奧海城第1期1樓高層地下UG07號舖",
            "telephone": "26260081",
            "product1": "0",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "電腦眼鏡店",
            "region": "九龍",
            "district": "尖沙咀",
            "address": "柯士甸道105號百安大廈地下Ｅ16舖",
            "telephone": "27238929",
            "product1": "1",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "瑞士眼鏡公司",
            "region": "九龍",
            "district": "尖沙咀",
            "address": "麼地道41W地下",
            "telephone": "27214248",
            "product1": "0",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "睛之選視光中心",
            "region": "九龍",
            "district": "彩虹",
            "address": "彩雲邨彩雲邨商場2樓A205號",
            "telephone": "21164811",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "名店專業眼鏡",
            "region": "九龍",
            "district": "慈雲山",
            "address": "慈雲山中心4樓418號舖",
            "telephone": "29406631",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "時尚眼鏡",
            "region": "九龍",
            "district": "慈雲山",
            "address": "毓華街23號慈雲山中心4樓413號舖",
            "telephone": "28037777",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "永佳眼鏡公司",
            "region": "九龍",
            "district": "新蒲崗",
            "address": "錦榮街地下2C號舖",
            "telephone": "23208439",
            "product1": "1",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "亞洲眼鏡城",
            "region": "九龍",
            "district": "新蒲崗",
            "address": "康強街5號地下",
            "telephone": "23514362",
            "product1": "0",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "豪門眼鏡",
            "region": "九龍",
            "district": "新蒲崗",
            "address": "崇齡街80A地下",
            "telephone": "23286611",
            "product1": "0",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "時尚眼鏡",
            "region": "九龍",
            "district": "新蒲崗",
            "address": "大有街34號新科技廣場6樓7-8室",
            "telephone": "28178800",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "天明眼鏡公司",
            "region": "九龍",
            "district": "旺角",
            "address": "上海街433號地下",
            "telephone": "23844077",
            "product1": "0",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "太子眼鏡",
            "region": "九龍",
            "district": "旺角",
            "address": "西洋菜南街258號長寧大廈地下F2",
            "telephone": "23812963",
            "product1": "0",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "目之舍眼鏡店",
            "region": "九龍",
            "district": "旺角",
            "address": "西洋菜街168電訊大廈2002室",
            "telephone": "27645018",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "好視力眼鏡中心",
            "region": "九龍",
            "district": "旺角",
            "address": "西洋菜街南2號A銀城廣場1001室",
            "telephone": "23801160",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "形象眼鏡",
            "region": "九龍",
            "district": "旺角",
            "address": "彌敦道610號荷李活商業中心8樓807室",
            "telephone": "23325081",
            "product1": "1",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "卓賢眼鏡公司",
            "region": "九龍",
            "district": "旺角",
            "address": "奶路臣街16號地下3號",
            "telephone": "27805848",
            "product1": "0",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "青玉視力中心",
            "region": "九龍",
            "district": "旺角",
            "address": "彌敦道701號番發大廈八樓",
            "telephone": "23678037",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "迪生眼鏡公司",
            "region": "九龍",
            "district": "旺角",
            "address": "彌敦道688號旺角中心1期11樓1120號舖",
            "telephone": "27588900",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "真視眼鏡",
            "region": "九龍",
            "district": "旺角",
            "address": "彌敦道610號荷利活商業中心16樓1620室",
            "telephone": "23884298",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "偉光眼鏡",
            "region": "九龍",
            "district": "旺角",
            "address": "弼街52-54號有成大廈4樓C",
            "telephone": "23956143",
            "product1": "1",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "眼鏡2000",
            "region": "九龍",
            "district": "旺角",
            "address": "西洋菜南街銀城廣場22樓2203室",
            "telephone": "23320851",
            "product1": "0",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "眼鏡2000",
            "region": "九龍",
            "district": "旺角",
            "address": "亞皆老街旺角中心1619室",
            "telephone": "27870016",
            "product1": "0",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "視力軒",
            "region": "九龍",
            "district": "旺角",
            "address": "洗衣街161號地下A",
            "telephone": "23932992",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "駿視眼鏡隱形眼鏡中心",
            "region": "九龍",
            "district": "旺角",
            "address": "亞皆老街98號地下7號舖",
            "telephone": "27122113",
            "product1": "0",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "麗誠眼鏡公司",
            "region": "九龍",
            "district": "旺角",
            "address": "花園街1號鴻禧大廈1樓2室",
            "telephone": "23887872",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "李鴻照視光師中心",
            "region": "九龍",
            "district": "旺角",
            "address": "彌敦道688號旺角中心1905室",
            "telephone": "23809000",
            "product1": "0",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "聯光眼鏡公司",
            "region": "九龍",
            "district": "旺角",
            "address": "上海街131號地下",
            "telephone": "23851217",
            "product1": "0",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "聯邦眼鏡公司",
            "region": "九龍",
            "district": "橫頭磡",
            "address": "樂富廣場2期1樓1205號舖",
            "telephone": "23389488",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "天然眼鏡中心",
            "region": "九龍",
            "district": "橫頭磡",
            "address": "樂富廣場3樓3214舖",
            "telephone": "23383765",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "麗晶眼鏡中心",
            "region": "九龍",
            "district": "橫頭磡",
            "address": "樂富廣場3樓3133DI舖",
            "telephone": "23381308",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "雅仕眼鏡",
            "region": "九龍",
            "district": "油塘",
            "address": "鯉魚門廣場1樓137舖",
            "telephone": "27131188",
            "product1": "1",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "眼鏡2000",
            "region": "九龍",
            "district": "深水埗",
            "address": "欽州街37K 西九龍中心2樓266號舖",
            "telephone": "27200533",
            "product1": "0",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "博視專業驗眼中心",
            "region": "九龍",
            "district": "深水埗",
            "address": "西九龍中心109號舖",
            "telephone": "27083312",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "焦點站",
            "region": "九龍",
            "district": "深水埗",
            "address": "大埔道10號地下舖",
            "telephone": "26172511",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "MC HOUSE",
            "region": "九龍",
            "district": "深水埗",
            "address": "欽州街37號5K 316號舖",
            "telephone": "96571576",
            "product1": "0",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "保視眼鏡中心",
            "region": "九龍",
            "district": "深水埗",
            "address": "欽洲街41號金寶樓地下1號舖",
            "telephone": "27891688",
            "product1": "0",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "雅圖電腦眼鏡公司",
            "region": "九龍",
            "district": "牛頭角",
            "address": "安基苑商場地下09號舖",
            "telephone": "27961682",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "瞳目眼鏡有限公司",
            "region": "九龍",
            "district": "牛頭角",
            "address": "牛頭角道77號淘大商場1期二樓S83號舖",
            "telephone": "26287272",
            "product1": "1",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "熊猫明星眼鏡有限公司",
            "region": "九龍",
            "district": "牛頭角",
            "address": "振華道70號樂華邨地下123號",
            "telephone": "27930328",
            "product1": "0",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "得寶專業眼鏡",
            "region": "九龍",
            "district": "牛頭角 ",
            "address": "牛頭角道3號得寶商場1樓216舖",
            "telephone": "26909190",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "大大電腦眼鏡中心",
            "region": "九龍",
            "district": "石硤尾",
            "address": "巴域街美山樓3號舖",
            "telephone": "27769318",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "大昌電腦眼鏡中心",
            "region": "九龍",
            "district": "石硤尾",
            "address": "大坑西邨民興樓4號地下",
            "telephone": "27784287",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "目廊專業驗眼中心",
            "region": "九龍",
            "district": "紅磡",
            "address": "黃埔新天地聚寶坊地庫一層KIDZONE B6舖",
            "telephone": "23659636",
            "product1": "1",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "豪盼軒",
            "region": "九龍",
            "district": "紅磡",
            "address": "紅磡灣中心地下19舖",
            "telephone": "27662888",
            "product1": "0",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "注目視光",
            "region": "九龍",
            "district": "紅磡",
            "address": "蕪湖街49號地下A-B舖",
            "telephone": "23335182",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "日昌眼鏡",
            "region": "九龍",
            "district": "美孚",
            "address": "美孚新邨萬事達廣場1樓N90號A舖",
            "telephone": "27865390",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "俊傑眼鏡",
            "region": "九龍",
            "district": "美孚",
            "address": "美孚新邨萬事達廣場成功商場56A地下",
            "telephone": "27863769",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "眼光獨到有限公司",
            "region": "九龍",
            "district": "美孚",
            "address": "美孚新邨3期吉利徑6-8號平台C1舖 ",
            "telephone": "31051231",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "俊傑眼鏡",
            "region": "九龍",
            "district": "荔枝角",
            "address": "景荔徑8號盈暉家居城第1層141號",
            "telephone": "31570153",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "目廊專業驗眼中心",
            "region": "九龍",
            "district": "藍田",
            "address": "匯景廣場L5層822A舗 ",
            "telephone": "29526082",
            "product1": "1",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "名店專業眼鏡",
            "region": "九龍",
            "district": "藍田",
            "address": "啟田邨啟田商場3樓302A號舖",
            "telephone": "23409833",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "名店專業眼鏡",
            "region": "九龍",
            "district": "藍田",
            "address": "德田邨德田廣場1樓118號舖",
            "telephone": "24747481",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "專業你的眼鏡",
            "region": "九龍",
            "district": "藍田",
            "address": "匯景道8號匯景廣場5樓948舖",
            "telephone": "22347777",
            "product1": "0",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "雅仕眼鏡",
            "region": "九龍",
            "district": "藍田",
            "address": "啟田邨啟田商場2樓213室",
            "telephone": "23886236",
            "product1": "1",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "懿然眼鏡",
            "region": "九龍",
            "district": "藍田",
            "address": "匯景廣場5樓822D室",
            "telephone": "22056198",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "視覺護理中心",
            "region": "九龍",
            "district": "藍田",
            "address": "匯景廣場5樓103室",
            "telephone": "27721330",
            "product1": "0",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "峰景視力",
            "region": "九龍",
            "district": "觀塘",
            "address": "秀茂坪秀茂坪邨商場地下9號",
            "telephone": "23496111",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "專業你的眼鏡",
            "region": "九龍",
            "district": "觀塘",
            "address": "協和街93-115建德大樓地下K舖",
            "telephone": "27260000",
            "product1": "0",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "彩視眼鏡",
            "region": "九龍",
            "district": "觀塘",
            "address": "秀茂坪寶達邨寶達商場1樓111號舖",
            "telephone": "21904396",
            "product1": "1",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "理想專業眼鏡",
            "region": "九龍",
            "district": "觀塘",
            "address": "觀塘道414號亞太中心19樓1902室",
            "telephone": "21543923",
            "product1": "0",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "眼鏡先生",
            "region": "九龍",
            "district": "觀塘",
            "address": "巧明街117號港貿中心6樓604室",
            "telephone": "36109173",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "眼鏡星球",
            "region": "九龍",
            "district": "觀塘",
            "address": "巧明街117號港貿中心12樓Workshop 5",
            "telephone": "23271000",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "準而佳眼鏡中心",
            "region": "九龍",
            "district": "觀塘",
            "address": "鴻圖道42號華寶中心21樓2107號",
            "telephone": "23804112",
            "product1": "0",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "睛之選視光中心",
            "region": "九龍",
            "district": "觀塘",
            "address": "秀茂坪安達邨安達商場LG08號舖",
            "telephone": "28680221",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "睛之選視光中心",
            "region": "九龍",
            "district": "觀塘",
            "address": "秀茂坪曉麗苑商場105號",
            "telephone": "31884521",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "緻目眼鏡",
            "region": "九龍",
            "district": "觀塘",
            "address": "開源道63號福昌大廈609室",
            "telephone": "27270618",
            "product1": "1",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "鄭偉雄眼科視光中心",
            "region": "九龍",
            "district": "觀塘",
            "address": "麗港城商場地下37-37A舖",
            "telephone": "27721223",
            "product1": "0",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "J VISION",
            "region": "九龍",
            "district": "觀塘",
            "address": "巧明街117號港貿中心17樓1701室",
            "telephone": "26683388",
            "product1": "0",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "囍+眼鏡",
            "region": "九龍",
            "district": "觀塘",
            "address": "秀茂坪秀茂坪邨商場1樓117號舖",
            "telephone": "28388892",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "名店專業眼鏡",
            "region": "九龍",
            "district": "鑽石山",
            "address": "鳳德商場201號舖",
            "telephone": "23255168",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "美視美專業眼鏡",
            "region": "九龍",
            "district": "長沙灣",
            "address": "青山道318號地下",
            "telephone": "28682001",
            "product1": "1",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "壹眼鏡店",
            "region": "九龍",
            "district": "長沙灣",
            "address": "保安道225號寶熙苑商場1號地下",
            "telephone": "23871323",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "傳真眼鏡專門店",
            "region": "九龍",
            "district": "長沙灣",
            "address": "青山道312號地下",
            "telephone": "27202606",
            "product1": "0",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "睛品專業眼鏡",
            "region": "九龍",
            "district": "長沙灣",
            "address": "元州街303號元州邨元州商場地下G15號舖",
            "telephone": "26625002",
            "product1": "0",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "比達眼鏡隱形眼鏡驗配中心",
            "region": "九龍",
            "district": "黃大仙",
            "address": "黃大仙中心南館地下G10K號舖 ",
            "telephone": "23219862",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "實惠眼鏡公司",
            "region": "九龍",
            "district": "黃大仙",
            "address": "竹園商場2樓S228舖",
            "telephone": "23267126",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "麗景眼鏡",
            "region": "九龍",
            "district": "黃大仙",
            "address": "鳳凰新邨蒲崗村道33號百利軒地下1F",
            "telephone": "23262295",
            "product1": "1",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "心目宗視力",
            "region": "九龍  ",
            "district": "黃大仙",
            "address": "黃大仙中心1樓118舖",
            "telephone": "23293698",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        }
    ],
    "新界": [
        {
            "company": "3D 眼鏡",
            "region": "新界",
            "district": "上水",
            "address": "龍琛路39號上水廣場7樓703A室",
            "telephone": "26730886",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "永明眼鏡專業公司",
            "region": "新界",
            "district": "上水",
            "address": "上水中心購物商場L2-2108號舖",
            "telephone": "26712523",
            "product1": "1",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "永明眼鏡專業公司",
            "region": "新界",
            "district": "上水",
            "address": "龍豐花園19號地舖",
            "telephone": "26739001",
            "product1": "1",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "名店專業眼鏡",
            "region": "新界",
            "district": "上水",
            "address": "彩園邨彩園廣場3樓29A號舖",
            "telephone": "26731132",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "名店專業眼鏡",
            "region": "新界",
            "district": "元朗",
            "address": "元朗大馬路225號地下",
            "telephone": "24787575",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "采視眼鏡中心",
            "region": "新界",
            "district": "元朗",
            "address": "朗屏邨商場L109號舖",
            "telephone": "21579828",
            "product1": "1",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "御目眼鏡",
            "region": "新界",
            "district": "元朗",
            "address": "青山公路元朗段22-26號金源大廈地下2號舖",
            "telephone": "24661898",
            "product1": "0",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "理想眼鏡",
            "region": "新界",
            "district": "元朗",
            "address": "大棠路23-27號合益廣場地下A座38",
            "telephone": "24702711",
            "product1": "0",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "博視專業驗眼中心",
            "region": "新界",
            "district": "元朗",
            "address": "形點II一樓A156舖",
            "telephone": "28118444",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "大圍專業護眼",
            "region": "新界 ",
            "district": "大圍",
            "address": "積富街49-55號安定樓地下G舖",
            "telephone": "35904411",
            "product1": "0",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "永明眼鏡專業公司",
            "region": "新界",
            "district": "大埔",
            "address": "大埔超級城B區地下135號舖",
            "telephone": "31254341",
            "product1": "1",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "昌明光學有限公司",
            "region": "新界",
            "district": "大埔",
            "address": "大埔墟廣福道124號A舖",
            "telephone": "26565719",
            "product1": "0",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "昌明眼鏡專門店",
            "region": "新界",
            "district": "大埔",
            "address": "安泰路1號大埔廣場L2樓5-JA舖",
            "telephone": "26573828",
            "product1": "0",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "勁視眼鏡店",
            "region": "新界",
            "district": "大埔",
            "address": "安慈路4號大埔昌運中心1樓99號",
            "telephone": "26672188",
            "product1": "0",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "高明眼鏡",
            "region": "新界",
            "district": "大埔",
            "address": "昌運中心2樓121號舖",
            "telephone": "26584365",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "創藝眼鏡中心",
            "region": "新界",
            "district": "大埔",
            "address": "大埔廣場2樓5F舖",
            "telephone": "26608843",
            "product1": "0",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "富視眼鏡公司",
            "region": "新界",
            "district": "大埔",
            "address": "安泰路1號大埔廣場麗晶廊40-10號地下",
            "telephone": "26677227",
            "product1": "1",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "視窗眼鏡",
            "region": "新界",
            "district": "大埔",
            "address": "大埔廣場2樓75號",
            "telephone": "26538883",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "視窗眼鏡",
            "region": "新界",
            "district": "大埔",
            "address": "昌運中心地下44A號",
            "telephone": "26672223",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "精藝視光中心",
            "region": "新界",
            "district": "大埔",
            "address": "大埔墟大明里11B號地下",
            "telephone": "26576212",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "醒目廊",
            "region": "新界",
            "district": "大埔",
            "address": "大埔廣場1樓40.21號舖",
            "telephone": "26660993",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "有眼福有限公司",
            "region": "新界",
            "district": "天水圍",
            "address": "嘉湖新北江商場1樓C96號",
            "telephone": "24489970",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "采視眼鏡中心",
            "region": "新界",
            "district": "天水圍",
            "address": "天澤邨天澤邨商場111號舖",
            "telephone": "24863634",
            "product1": "1",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "囍+眼鏡",
            "region": "新界",
            "district": "天水圍",
            "address": "天秀路8號天一商場3樓3005號",
            "telephone": "28891853",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "利奧專業眼鏡",
            "region": "新界",
            "district": "天水圍",
            "address": "天水圍天喜街9號天慈商場地下1B",
            "telephone": "21690979",
            "product1": "1",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "TWENTY TWENTY",
            "region": "新界",
            "district": "天水圍",
            "address": "天澤商場1樓105E",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "有眼福有限公司",
            "region": "新界",
            "district": "天水圍",
            "address": "天耀廣場地下L040A號舖",
            "telephone": "21160362",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "日昌眼鏡",
            "region": "新界",
            "district": "將軍澳",
            "address": "坑口培成路15號連理街139B舖",
            "telephone": "21410066",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "眼鏡2000",
            "region": "新界",
            "district": "將軍澳",
            "address": "將軍澳廣場1樓1-138號舖",
            "telephone": "23883370",
            "product1": "0",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "眼鏡廊",
            "region": "新界",
            "district": "將軍澳",
            "address": "南豐廣場地下A47號舖",
            "telephone": "24350333",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "博視專業驗眼中心",
            "region": "新界",
            "district": "將軍澳",
            "address": "新都城中心商場3期商場L1層104舖",
            "telephone": "31943101",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "視覺廊",
            "region": "新界",
            "district": "將軍澳",
            "address": "新都城第3期130A舖",
            "telephone": "31946619",
            "product1": "1",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "VISUAL PLUS",
            "region": "新界",
            "district": "將軍澳",
            "address": "調景嶺彩明街1號彩明商場二樓241號舖",
            "telephone": "29526663",
            "product1": "1",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "3D 眼鏡",
            "region": "新界",
            "district": "屯門",
            "address": "屯喜路2號屯門柏麗廣場17樓1720室",
            "telephone": "21468449",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "Q8眼鏡",
            "region": "新界",
            "district": "屯門",
            "address": "華都花園商場3樓8P舖位",
            "telephone": "24503218",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "采視眼鏡中心",
            "region": "新界",
            "district": "屯門",
            "address": "良景邨良景商場L338A號舖",
            "telephone": "24031222",
            "product1": "1",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "添寶眼鏡",
            "region": "新界",
            "district": "屯門",
            "address": "H.A.N.D.S A區1樓A-141號舖 ",
            "telephone": "24515984",
            "product1": "0",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "眼鏡一族",
            "region": "新界",
            "district": "屯門",
            "address": "屯喜路2號屯門栢麗廣場2樓219號舖",
            "telephone": "24522935",
            "product1": "0",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "眼鏡典範",
            "region": "新界",
            "district": "屯門",
            "address": "仁愛堂街1-43號麗日閣地下17號舖",
            "telephone": "24402636",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "E度眼鏡",
            "region": "新界",
            "district": "屯門",
            "address": "湖翠路138號啟豐園1樓12號舖",
            "telephone": "29885538",
            "product1": "0",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "J VISION",
            "region": "新界",
            "district": "屯門",
            "address": "屯喜路2號屯門柏麗廣場19樓03室",
            "telephone": "24599678",
            "product1": "0",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "名店專業眼鏡",
            "region": "新界",
            "district": "屯門",
            "address": "屯門時代廣場南翼, L2 二樓, 7舖",
            "telephone": "24524086",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "KING'S VISION",
            "region": "新界",
            "district": "屯門",
            "address": "龍門路55號新屯門商場3樓177號舖",
            "telephone": "24692323",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "眼鏡巨人",
            "region": "新界",
            "district": "屯門",
            "address": "華都花園地下8號舖",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "實惠眼鏡公司",
            "region": "新界",
            "district": "東涌",
            "address": "逸東邨逸東商場2樓212舖",
            "telephone": "21519629",
            "product1": "1",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "金葉眼鏡公司",
            "region": "新界",
            "district": "沙田",
            "address": "銀城街1號置富第一城146B舖",
            "telephone": "26483472",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "清目視力",
            "region": "新界",
            "district": "沙田",
            "address": "小瀝源牛皮沙街2號愉翠商場1樓106號舖",
            "telephone": "31631099",
            "product1": "1",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "眼鏡廊",
            "region": "新界",
            "district": "沙田",
            "address": "禾輋邨禾輋商場2樓235號舖",
            "telephone": "23827998",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "眼鏡廊",
            "region": "新界",
            "district": "沙田",
            "address": "沙角邨沙角商場236-237號",
            "telephone": "26370438",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "寶光眼鏡",
            "region": "新界",
            "district": "沙田",
            "address": "大圍美林商場1樓62 & 81號舖",
            "telephone": "26828840",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "寶光眼鏡公司",
            "region": "新界",
            "district": "沙田",
            "address": "沙角商場216號",
            "telephone": "26470565",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "J VISION",
            "region": "新界",
            "district": "沙田",
            "address": "連城廣場6樓621-622室",
            "telephone": "26811829",
            "product1": "0",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "新法視光學專門店",
            "region": "新界",
            "district": "沙田",
            "address": "瀝源村富裕樓1樓5室",
            "telephone": "26067980",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "永明眼鏡專業公司",
            "region": "新界",
            "district": "粉嶺",
            "address": "碧湖花園商場UG13號舖",
            "telephone": "37410468",
            "product1": "1",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "新天地視光中心",
            "region": "新界",
            "district": "粉嶺",
            "address": "百和路88號花都廣場A50號地舖",
            "telephone": "26775430",
            "product1": "1",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "實惠眼鏡公司",
            "region": "新界",
            "district": "粉嶺",
            "address": "百和路88號花都廣場A61舖",
            "telephone": "26832392",
            "product1": "1",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "奧比眼鏡",
            "region": "新界",
            "district": "粉嶺",
            "address": "百和路88號花都廣場A18號舖",
            "telephone": "26691418",
            "product1": "1",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "衛視眼鏡公司",
            "region": "新界",
            "district": "粉嶺",
            "address": "粉嶺新運路33號粉嶺中心1樓213B&C",
            "telephone": "26772881",
            "product1": "0",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "目廊專業驗眼中心",
            "region": "新界",
            "district": "粉嶺",
            "address": "粉嶺中心1樓256號舖",
            "telephone": "27912638",
            "product1": "1",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "1 + 1 專業眼鏡",
            "region": "新界",
            "district": "荃灣",
            "address": "楊屋道1號荃新天地1樓127A號舖",
            "telephone": "29410238",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "3D 眼鏡",
            "region": "新界",
            "district": "荃灣",
            "address": "眾安街68號荃灣千色匯1期1樓1025舖",
            "telephone": "29407768",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "EYECARE PLUS",
            "region": "新界",
            "district": "荃灣",
            "address": "南豐中心23樓2301C-2G室",
            "telephone": "39568614",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "田氏眼鏡公司",
            "region": "新界",
            "district": "荃灣",
            "address": "青山公路荃灣段269號富裕樓力生廣場地下G11舖",
            "telephone": "24168824",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "名店專業眼鏡",
            "region": "新界",
            "district": "荃灣",
            "address": "眾安街68號荃灣千色匯地下G015號",
            "telephone": "29406630",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "利誠眼鏡",
            "region": "新界",
            "district": "荃灣",
            "address": "綠楊坊1樓F8室",
            "telephone": "24982468",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "時尚眼鏡",
            "region": "新界",
            "district": "荃灣",
            "address": "西樓角路138-168號荃豐中心1樓B7舖",
            "telephone": "28027777",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "時尚眼鏡",
            "region": "新界",
            "district": "荃灣",
            "address": "悅來坊悅來商場B112A舖",
            "telephone": "23827777",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "時尚眼鏡",
            "region": "新界",
            "district": "荃灣",
            "address": "眾安街68號荃灣千色匯1期1樓1015-1016舖",
            "telephone": "24997777",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "荃新視力",
            "region": "新界",
            "district": "荃灣",
            "address": "海濱花園A座地下105號地舖",
            "telephone": "24081083",
            "product1": "0",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "眼前一亮",
            "region": "新界",
            "district": "荃灣",
            "address": "青山公路荃灣段625號麗城商場3期24號地舖",
            "telephone": "26990068",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "新時代眼鏡",
            "region": "新界",
            "district": "荃灣",
            "address": "眾安街68號荃灣千色匯1期1樓1039舖",
            "telephone": "29407193",
            "product1": "0",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "睛點眼鏡",
            "region": "新界",
            "district": "荃灣",
            "address": "南豐中心6樓631室",
            "telephone": "28853878",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "優點視力",
            "region": "新界",
            "district": "荃灣",
            "address": "南豐中心10樓1028室",
            "telephone": "28853030",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "J VISION",
            "region": "新界",
            "district": "荃灣",
            "address": "千色匯1期寫字樓22樓2207-8室",
            "telephone": "29407728",
            "product1": "0",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "LIKE OPTICAL",
            "region": "新界",
            "district": "葵涌",
            "address": "葵涌廣場2樓C107舖",
            "telephone": "21165330",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "光威眼鏡",
            "region": "新界",
            "district": "葵涌",
            "address": "和宜合道180號地下",
            "telephone": "24279809",
            "product1": "0",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "有眼福有限公司",
            "region": "新界",
            "district": "葵涌",
            "address": "葵涌廣場2樓C67B舖",
            "telephone": "26100345",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "有眼福有限公司",
            "region": "新界",
            "district": "葵涌",
            "address": "葵涌廣場地下A77-2舖",
            "telephone": "21160362",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "峰景視力",
            "region": "新界",
            "district": "葵涌",
            "address": "石籬商場2樓237號舖",
            "telephone": "28893068",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "瑞視眼鏡",
            "region": "新界",
            "district": "葵涌",
            "address": "和宜合道26-30號和宜商場地下21號舖",
            "telephone": "24289968",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "葵興眼鏡公司",
            "region": "新界",
            "district": "葵興",
            "address": "葵芳路166-174號 新葵興商場2樓39舖",
            "telephone": "24851492",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "M OPTICAL",
            "region": "新界",
            "district": "葵芳",
            "address": "葵涌廣場2樓113A號舖",
            "telephone": "24281028",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "康聯視力中心",
            "region": "新界",
            "district": "葵芳",
            "address": "葵芳邨葵芳商場106號舖",
            "telephone": "24071113",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "都會視力中心",
            "region": "新界",
            "district": "葵芳",
            "address": "新都會廣場4樓409號",
            "telephone": "24853889",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "新眼鏡繽紛",
            "region": "新界",
            "district": "葵芳",
            "address": "葵芳邨葵愛樓16號地下",
            "telephone": "24090338",
            "product1": "1",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "博視專業驗眼中心",
            "region": "新界",
            "district": "調景嶺",
            "address": "都會駅商場L2層2045 D&E舗",
            "telephone": "22070012",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "日昌眼鏡",
            "region": "新界",
            "district": "青衣",
            "address": "青衣機鐵站2樓205-6號舖",
            "telephone": "26886655",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "利誠眼鏡",
            "region": "新界",
            "district": "青衣",
            "address": "青衣機鐵站34號舖",
            "telephone": "24971992",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "眼鏡廊",
            "region": "新界",
            "district": "青衣",
            "address": "長發商場220號舗",
            "telephone": "24360031",
            "product1": "1",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "VISUAL PLUS",
            "region": "新界",
            "district": "馬鞍山",
            "address": "馬鞍山中心地下A16號舖",
            "telephone": "22546878",
            "product1": "1",
            "product2": "1",
            "product3": "0"
        },
        {
            "company": "俊傑眼鏡",
            "region": "新界",
            "district": "馬鞍山",
            "address": "鞍誠街28號富輝花園商場1樓14號舖",
            "telephone": "26309955",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "眼鏡2000",
            "region": "新界",
            "district": "馬鞍山",
            "address": "新港城中心商場2樓2109號舖",
            "telephone": "26338049",
            "product1": "0",
            "product2": "1",
            "product3": "1"
        },
        {
            "company": "博視專業驗眼中心",
            "region": "新界",
            "district": "馬鞍山",
            "address": "西沙路608號馬鞍山廣場3樓339A舖",
            "telephone": "26348909",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "博視專業驗眼中心",
            "region": "新界",
            "district": "馬鞍山",
            "address": "鞍祿街18號新港城中心L2層2E-87舖",
            "telephone": "21110698",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "名悅眼鏡",
            "region": "新界",
            "district": "馬鞍山",
            "address": "恆安商場2樓205號舖",
            "telephone": "26423820",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        }
    ],
    "澳門": [
        {
            "company": "亮點視力",
            "region": "澳門",
            "district": "澳門",
            "address": "連勝馬路58-A號地下C座",
            "telephone": "28350061",
            "product1": "0",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "利高眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門羅利老馬路32號德群大廈地下",
            "telephone": "28368118",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "利奧眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門高地烏街31E號地下",
            "telephone": "28554509",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "卓越眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門俾利喇街108號地下",
            "telephone": "28527316",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "卓越眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門伯多祿局長街5號銀座廣場地下",
            "telephone": "28322032",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "卓越眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "氹仔埃武拉街295號地下",
            "telephone": "28842922",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "名亮眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門高士德大馬路60B號祐美大廈(第七座)地下",
            "telephone": "28529280",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "名亮眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門俾利喇街82-B號,利暉大廈第三座地下B座",
            "telephone": "28529055",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "名視眼鏡光學中心",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門羅利老馬路17-17A號地下",
            "telephone": "28368633",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "名店眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門白朗古將軍大馬路77號地下",
            "telephone": "28225456",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "名店眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門亞利鴉架街1D號地下D舖",
            "telephone": "28216336",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "真誠眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門十月初五日街15號地下",
            "telephone": "28956121",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "真誠眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門河邊新街85A號地下",
            "telephone": "28939844",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "真誠眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門高仕德大馬路9E世紀豪庭1樓T舖",
            "telephone": "28454403",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "真誠眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門提督馬路46號地下",
            "telephone": "28259915",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "專業眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門水坑尾街311號金興大廈地下",
            "telephone": "28353366",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "專業眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門賣草地街7號地下",
            "telephone": "28355118",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "捷鷹光學眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門黑沙環新街204號建華大廈(第六座)地下",
            "telephone": "28452533",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "捷鷹光學眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門關閘看臺街127號地下",
            "telephone": "28435006",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "新形象眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門俾利喇街78B號利輝大廈地下",
            "telephone": "28527920",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "新形象眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門西墳馬路1-C號富安大廈地下",
            "telephone": "28523543",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "新潮眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門祐漢新村第一街29號地下",
            "telephone": "28403397",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "新潮眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門祐漢新村第六街84號黃金商場BS舖",
            "telephone": "28415303",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "新潮眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門黑沙環中街383號廣福祥花園(第七座)地下",
            "telephone": "28454381",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "新潮眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門黑沙環馬路34號地下",
            "telephone": "28471797",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "新潮眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門黑沙環新街409號廣華新邨(第十三座)地下",
            "telephone": "28761248",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "新潮眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門河邊新街88-A海天大廈A鋪",
            "telephone": "28932731",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "詩樂眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門渡船街34號寶仁大廈地下",
            "telephone": "28211808",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "詩樂眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門黑沙環馬路3號岐關新邨(第五座)地下",
            "telephone": "28531382",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "詩樂眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門鏡湖馬路97號雅昇樓地下",
            "telephone": "28311415",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "優悅眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門羅利老馬路18B號地下",
            "telephone": "28529419",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "世紀眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門新馬路219號地下",
            "telephone": "28574145",
            "product1": "0",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "玉福眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門天神巷13號金富利大廈地下",
            "telephone": "28322166",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "玉福眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門草堆街112B號明裕大廈地下C座",
            "telephone": "28353568",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "玉福眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門高士德大馬路62號地下",
            "telephone": "28525089",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "玉福眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門黑沙環馬路3C號地下",
            "telephone": "28521572",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "玉福眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門氹仔大連街432號",
            "telephone": "28833083",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "先鋒眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "板樟堂德香里8號地下",
            "telephone": "28355682",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "繁榮眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門高仕德大馬路87號地下",
            "telephone": "28384872",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "繁榮眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門連勝馬路68號地下",
            "telephone": "28371039",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "佳藝眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門天神巷47號國華戲院商場地下",
            "telephone": "28519888",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "皇視眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門製造廠巷26號南藝閣地下",
            "telephone": "28531131",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "盈亮眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門倫斯泰特大馬路346號獲多利大廈地下",
            "telephone": "28889812",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "盈安眼科醫療中心",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門南灣大馬路429號南灣商業中心1樓",
            "telephone": "28338836",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "佳藝眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門河邊新街22A號信業大廈地下",
            "telephone": "28939412",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "佳藝眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門黑沙環馬路5A號岐關新邨地下",
            "telephone": "28520359",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "保光視力",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門賈伯樂提督街 43-C地下",
            "telephone": "28526622",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "保視眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門高士德大馬路99號地下",
            "telephone": "28215590",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "明藝眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門黑沙環馬路21D號南藝閣地下",
            "telephone": "28518288",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "高清眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門青洲大馬路487號美居廣場地下",
            "telephone": "28222663",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "高清眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門鏡湖馬路49號惠緻苑地下",
            "telephone": "28352136",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "高清眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門祐漢新村第八街44號吉祥樓地下",
            "telephone": "63233272",
            "product1": "0",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "高清視力",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門高士德大馬路88號地下",
            "telephone": "28825678",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "澳視眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門高仕德大馬路45-B地下",
            "telephone": "28556388",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "臻美眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "河邊新街鹽里15L永發大廈地下E座",
            "telephone": "28592554",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "袁介眼科中心",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門連勝馬路15號金昌大廈地下",
            "telephone": "28361591",
            "product1": "0",
            "product2": "0",
            "product3": "0"
        },
        {
            "company": "焦點眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門羅神父街15號泉亨花園地下",
            "telephone": "28550299",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "眼鏡廊",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門新馬路99號南華商業大廈地下",
            "telephone": "28323673",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "睛品眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "青洲大馬路逸麗花園182號地下K舖",
            "telephone": "28761749",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "名亮眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門連勝馬路133號",
            "telephone": "28556063",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "高清眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門祐漢市場街540-B槳富新村地下",
            "telephone": "28413113",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "優悅眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門羅白沙街23號昌明花園第一期(寶星閣)地下",
            "telephone": "28217704",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "視覺空間",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門馬場北大馬路信達廣場二座地下P00-33號",
            "telephone": "63008208",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "天幕眼鏡電訊專賣店",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門天神巷11號地下",
            "telephone": "28337890",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "臻美眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門河邊新街鹽里15L永發大廈地下E座",
            "telephone": "28592554",
            "product1": "1",
            "product2": "0",
            "product3": "1"
        },
        {
            "company": "卓越眼鏡",
            "region": "澳門",
            "district": "澳門",
            "address": "澳門大三巴街藻輝閣地下F舖",
            "telephone": "28322006",
            "product1": "1",
            "product2": "0",
            "product3": "0"
        }
    ]
};