<?php
class Registration extends FIT_Controller {
    public function __construct(){
        parent::__construct(true);
        $this->load->model('registration_model');
        $this->load->model('auth_model');

    }
    public function getList($params = null)
    {
        // $this->checkHttpMethod('GET');
        // $this->setParams($param, $this->input->get());
        $param = $this->input->get();
        $this->user = $this->auth_model->auth();
        $data = $this->registration_model->getList($param, $this->user);
        $this->util->success($data);
    }
    public function submit($params = null)
    {
        // $this->checkHttpMethod('POST');
        $param = $this->input->post();
        $data = $this->registration_model->submit($param);
        $this->util->success($data);
    }
    public function verify($params = null)
    {
        // $this->checkHttpMethod('POST');
        $param = $this->input->post();
        $data = $this->registration_model->isValid($param);
        $this->util->success($data);
    }
    public function delete($params = null)
    {
        // $this->checkHttpMethod('POST');
        $param = $this->input->post();
        $this->user = $this->auth_model->auth();
        $data = $this->registration_model->delete($param);
        $this->util->success($data);
    }
    public function redeem($params = null)
    {
        $this->checkHttpMethod('POST');
        $param = $this->input->post();
        $this->user = $this->auth_model->auth();
        $data = $this->registration_model->redeem($param, $this->user);
        $this->util->success($data);
    }

    public function redeemLite($params = null)
    {
        // $this->checkHttpMethod('POST');
        $param = $this->input->post();
        $data = $this->registration_model->redeemLite($param);
        $this->util->success($data);
    }

       public function promoteCodeVerify($params = null)
    {
        // $this->checkHttpMethod('POST');
        $param = $this->input->post();
        $data = $this->registration_model->promoteCodeVerify($param['promoteCode']);
        $this->util->success($data);
    }
       public function promoteCodeVerifyExpired($params = null)
    {
        // $this->checkHttpMethod('POST');
        $param = $this->input->post();
        $data = $this->registration_model->promoteCodeVerifyExpired($param['promoteCode']);
        $this->util->success($data);
    }
    public function email($params = null ){
        $param = $this->input->post();
        $data = $this->registration_model->emailService($param);
        $this->util->success($data);
    }
    public function export($params = null){
        $param = $this->input->get();
        // $this->user = $this->auth_model->auth();
        $data = $this->registration_model->export($param);
        $this->exportCSV('registrationList.csv',$data);
    }
    public function contactNumber($params = null){
        $param = $this->input->post();
        $data = $this->registration_model->contactNumber($param);
        $this->util->success($data);
    }
    public function updateDistrictParentkey($params = null){
        $param = $this->input->post();
        $this->user = $this->auth_model->auth();
        $data = $this->registration_model->updateDistrictParentkey($param);
        $this->util->success($data);

    }
    // public function getStoreByUser($user){
    //     $param = $this->input->post();
    //     $data = $this->registration_model->getStoreByUser($param);
    //     $this->util->success($data);

    // }

}
