<?php
class Pages extends FIT_Controller {
   public function __construct(){
        parent::__construct(true);
        $this->load->model('registration_model');
    }
        public function view($page = 'home')
        {

        if ( ! file_exists(APPPATH.'views/pages/'.$page.'.php'))
        {
                // Whoops, we don't have a page for that!
                show_404();
        }

        $data['title'] = ucfirst($page); // Capitalize the first letter

        $this->load->view('templates/header', $data);
        $this->load->view('pages/'.$page, $data);
        $this->load->view('templates/footer', $data);

        }

        public function email(){
        
                $query = $this->db->query("
                 SELECT r.id, r.name, r.email, r.telephone, r.promoteCode,
                 s4.text as 'region', s5.text as 'district', s6.name as 'store', s6.address as 'storeAddress', s6.telephone as 'storeTelephone'
                 From registration r
                 LEFT JOIN selection s4 on s4.value = r.region 
                 LEFT JOIN selection s5 ON s5.value = r.district and s5.parentKey = r.region 
                 LEFT JOIN store s6 ON s6.ID = r.store and s6.parentKey = r.district 
                 WHERE r.id = 84
                 ")->row_array();  


         $this->load->view('templates/emailTemplate', $query);

        }
}
