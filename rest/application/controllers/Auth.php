<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends FIT_Controller {

    function __construct() 
    { 
        parent::__construct(false); 
        $this->load->model('auth_model');
    } 

    public function login()
    {
        $this->checkHttpMethod('POST');

        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $result = $this->auth_model->login($username,$password);

        $this->success($result);
    }

    public function register()
    {
        $this->checkHttpMethod('POST');
        // $this->setParams($param, $this->input->post());
        $param['username'] = $this->input->post('username');
        $param['role'] = $this->input->post('role');
        $data = $this->auth_model->register($param);
        $this->success($data);
    }

        public function auth()
    {
        $this->checkHttpMethod('POST');
        // $this->setParams($param, $this->input->post());
        $data = $this->auth_model->auth();
        $this->success($data);
    }

    public function multiRegister()  // Create Merchant, $param = { username: '' }
    {
        $this->checkHttpMethod('POST');
        $this->user = $this->auth_model->auth();
        $param = $this->input->post();
        $data = $this->auth_model->multiRegister($param);
        $this->success($data);
    }

    public function getMerchantList(){  // Retrieve Merchant
        $this->checkHttpMethod('GET');
        $this->user = $this->auth_model->auth();
        $param = $this->input->get();
        $data = $this->auth_model->getMerchantList($param);
        $this->success($data);
    }

    // public function sendCode()
    // {
    //     $this->checkHttpMethod('POST');
    //     $this->setParams($param, $this->input->post());
    //     $data = $this->auth_model->sendCode($param);
    //     $this->success($data);          
    // }

    // public function verifyMobile()
    // {
    //     $this->checkHttpMethod('POST');
    //     $this->setParams($param, $this->input->post());
    //     $data = $this->auth_model->verifyMobile($param);
    //     $this->success($data);          
    // }

    // public function createToken()
    // {
    //     $this->checkHttpMethod('POST');
    //     $this->setParams($param, $this->input->post());
    //     $data = $this->auth_model->createToken($param);
    //     $this->success($data);          
    // }

    // public function generateReferer()
    // {
    //     $this->checkHttpMethod('POST');
    //     $this->setParams($param, $this->input->post());
    //     $data = $this->auth_model->generateReferer($param['from'], $param['to']);
    //     $this->success($data);          
    // }
}


