<?php
class Redemption extends FIT_Controller {
    public function __construct(){
        parent::__construct(true);
        $this->load->model('redemption_model');
        $this->load->model('auth_model');
    }
    public function getList($params = null)
    {
        // $this->checkHttpMethod('GET');
        // $this->setParams($param, $this->input->get());
        $param = $this->input->get();
        $this->user = $this->auth_model->auth();
        $data = $this->redemption_model->getList($param, $this->user);
        $this->util->success($data);
    }
   
    public function delete($params = null)
    {
        // $this->checkHttpMethod('POST');
        $param = $this->input->post();
        $this->user = $this->auth_model->auth();
        $data = $this->redemption_model->delete($param);
        $this->util->success($data);
    }
   
    public function export($params = null){
        $param = $this->input->get();
        // $this->user = $this->auth_model->auth();
        $data = $this->redemption_model->export($param);
        $this->exportCSV('redemptionList.csv',$data);
    }
}
