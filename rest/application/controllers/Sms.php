<?php
class Sms extends FIT_Controller {

    public function __construct(){
        parent::__construct(true);
        $this->load->model('sms_model');
        $this->load->model('auth_model');
    }

    //   public function get($params = null)
    // {
    //     $data = $this->selection_model->getSelectionList();
    //     $this->util->success($data);
    // }

  function _remap($method_name = 'selectionList'){

             if(!method_exists($this, $method_name)){
                $this->selectionList();
             }
             else{
                $this->{$method_name}();
             }
         }

    public function getCustomerList()
    {
        $param = $this->input->get();
        $data = $this->sms_model->getCustomerList($param);
        $this->util->success($data);
    }
    public function previewMessage()
    {
        $param = $this->input->get();
        $data = $this->sms_model->previewMessage($param);
        $this->util->success($data);
    }
    public function reconnectCustomer()
    {
        $param = $this->input->post();
        $this->user = $this->auth_model->auth();
        $data = $this->sms_model->reconnectCustomer($param);
        $this->util->success($data);
    }
    public function tailorMessage()
    {
        $param = $this->input->post();
        $this->user = $this->auth_model->auth();
        $data = $this->sms_model->tailorMessage($param);
        $this->util->success($data);
    }
}
