<?php
class Store extends FIT_Controller {

    public function __construct(){
        parent::__construct(true);
        $this->load->model('store_model');
        $this->load->model('selection_model');
        $this->load->model('auth_model');

    }
    // Registration Store List Module(A)

    public function createStore()
    {
        $param = $this->input->post();
        $this->user = $this->auth_model->auth();
        $data = $this->store_model->createStore($param);
        $this->util->success($data);
    }
    public function updateStore()
    {
        $param = $this->input->post();
        $this->user = $this->auth_model->auth();
        $data = $this->store_model->updateStore($param);
        $this->util->success($data);
    }
    public function getList(){
        $param = $this->input->get();
        // $this->user = $this->auth_model->auth();
        $data = $this->store_model->getList($param);
        $this->util->success($data);
    }

    public function delete()
    {
       $param = $this->input->post();
        $this->user = $this->auth_model->auth();
        $data = $this->store_model->delete($param);
        $this->util->success($data);
    }
    public function export()
    {
        $param = $this->input->get();
        $data = $this->store_model->export($param);
        $this->exportCSV('storeList.csv',$data);
    }

    // Static Store List Module(B) - For List View

     public function getStaticList(){
        $param = $this->input->get();
        // $this->user = $this->auth_model->auth();
        $data = $this->store_model->getStaticStoreList($param);
        $this->util->success($data);
    }

        public function createStaticStore()
    {
        $param = $this->input->post();
        $this->user = $this->auth_model->auth();
        $data = $this->store_model->createStaticStore($param);
        $this->util->success($data);
    }
    public function updateStaticStore()
    {
        $param = $this->input->post();
        $this->user = $this->auth_model->auth();
        $data = $this->store_model->updateStaticStore($param);
        $this->util->success($data);
    }

    public function deleteStaticStore()
    {
       $param = $this->input->post();
        $this->user = $this->auth_model->auth();
        $data = $this->store_model->deleteStaticStore($param);
        $this->util->success($data);
    }
    public function exportStaticStore()
    {
        $param = $this->input->get();
        $data = $this->store_model->exportStaticStore($param);
        $this->exportCSV('table_store_list.csv',$data);
    }

     public function getFinalList(){
        $param = $this->input->get();
        // $this->user = $this->auth_model->auth();
        $data = $this->store_model->getFinalStoreList($param);
        // $data = "abc";
        $this->util->success($data);
    }

     public function getFinalStoreDetail(){
        $param = $this->input->get();
        // $this->user = $this->auth_model->auth();
        $data = $this->store_model->getFinalStoreDetail($param);
        // $data = "abc";
        $this->util->success($data);
    }

    public function createFinalStore()
    {
        $param = $this->input->post();
        $this->user = $this->auth_model->auth();
        $data = $this->store_model->createFinalStore($param);
        $this->util->success($data);
    }
    public function updateFinalStore()
    {
        $param = $this->input->post();
        $this->user = $this->auth_model->auth();
        $data = $this->store_model->updateFinalStore($param);
        $this->util->success($data);
    }

    public function deleteFinalStore()
    {
       $param = $this->input->post();
        $this->user = $this->auth_model->auth();
        $data = $this->store_model->deleteFinalStore($param);
        $this->util->success($data);
    }
    public function exportFinalStore()
    {
        $param = $this->input->get();
        $data = $this->store_model->exportFinalStore($param);
        $this->exportCSV('table_store_list.csv',$data);
    }

}
