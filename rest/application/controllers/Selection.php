<?php
class Selection extends FIT_Controller {

    public function __construct(){
        parent::__construct(true);
        $this->load->model('selection_model');

    }

    //   public function get($params = null)
    // {
    //     $data = $this->selection_model->getSelectionList();
    //     $this->util->success($data);
    // }

  function _remap($method_name = 'selectionList'){

             if(!method_exists($this, $method_name)){
                $this->selectionList();
             }
             else{
                $this->{$method_name}();
             }
         }

    public function selectionList()
    {
        $data = $this->selection_model->getSelectionList();
        $this->util->success($data);
    }
    public function storeList()
    {
        // $param = $this->input->get();
        $data = $this->selection_model->getStoreList();
        $this->util->success($data);
    }
}
