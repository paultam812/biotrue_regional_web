<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Email_sender
{
            const SMTP_HOST = "mail.s213.sureserver.com";
            const SMTP_PORT = 587;
            const SMTP_USER = "info@p4p.com.hk";
            const SMTP_PASS = "91740469";
            const FROM_USER = "Biotrue";
            const FROM_EMAIL = "MKTG.BLHK@bausch.com";

    // const SMTP_HOST = "smtp.bausch.com";
    // const SMTP_PORT = 25;
    // const FROM_USER = "iBest System Info";
    // const FROM_EMAIL = "info@bausch.com";


    const REQUIRE_FIELDS = array(
        "forgetPassword" => array("userName", "newPassword", "title", "lastName", "resetTime"),
        "welcomeLetter" => array("policy"),
        "agentRegister" => array("password", "agentCode", "title", "lastName")
    );

    public $CI;
    public function __construct()
    {
        $this->CI = & get_instance();
    }

    public function sendEmail($subject, $content, $to, $from)
    {
        if (empty($from)) {
            $from = self::FROM_EMAIL;
        }

        if (!$this->isEmailValid($to) || !$this->isEmailValid($from) || empty($content)) {
            return false;
        }

        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => self::SMTP_HOST,
            'smtp_port' => self::SMTP_PORT,
            'smtp_user' => self::SMTP_USER,
            'smtp_pass' => self::SMTP_PASS,
            'mailtype' => 'html',
            'charset' => 'utf-8',
            'wordwrap' => true,
            'crlf' => "\r\n"
        );

        $this->CI->load->library('email', $config);
        $this->CI->email->set_newline("\r\n");
        $this->CI->email->from($from, self::FROM_USER);
        $this->CI->email->to($to);
        $this->CI->email->subject($subject);
        $this->CI->email->message($content);
        if ($this->CI->email->send()) {
            return true;
        } else {
            return false;
        }
    }

    public function loadSubject($type)
    {
        $langKey = $this->mapLangKey($type);
        return $this->CI->lang($langKey)['subject'];
    }

    public function loadEmailTemplate($type, $param)
    {
        $lang = $this->CI->lang->is_loaded['email_lang.php'];
        $langKey = $this->mapLangKey($type);

        $data['title'] = $this->CI->lang($langKey)['title'];
        $data['preheader'] = $this->CI->lang($langKey)['preheader'];
        $data['info'] = $this->CI->lang('email_info');
    
        if ($this->checkTemplateParam($type, $param)) {
            $data['param'] = $param;
            $data['content'] =$this->CI->load->view("emails/$lang/$type", $data['param'], true);
            $content = $this->CI->load->view('emails/emailTemplate', $data, true);
        } else {
            $content = '';
        }

        return $content;
    }

    private function mapLangKey($type)
    {
        return 'email_'.$this->CI->toSnakeCase($type, 'lower');
    }

    private function checkTemplateParam($type, $param)
    {
        $lang = $this->CI->lang->is_loaded['email_lang.php'];

        if (!file_exists(APPPATH.'views/emails/'.$lang.'/'.$type.'.php')) {
            return false;
        }

        $requiredFields = self::REQUIRE_FIELDS[$type];

        foreach ($requiredFields as $field) {
            if (!in_array($field, array_keys($param))) {
                return false;
            }
        }
        return true;
    }

    private function isEmailValid($email)
    {
        return empty(filter_var($email, FILTER_VALIDATE_EMAIL)) ? false : true;
    }

    public function getRequiredFields($type)
    {
        if (array_key_exists($type, self::REQUIRE_FIELDS)) {
            return self::REQUIRE_FIELDS[$type];
        } else {
            return 'Invalid Key';
        }
    }

    public function getAllTypes()
    {
        return self::REQUIRE_FIELDS;
    }
}
