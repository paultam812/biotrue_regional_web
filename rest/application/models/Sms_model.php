<?php
class Sms_model extends FIT_Model
{
    public function __construct(){
        parent::__construct('sms');

    }
    public function getCustomerList( $params = null )
    {
        $selection = $this->db->query('select * from registration')->result_array();
        $data['list'] = $selection;
        $data['total'] = sizeof($selection);

      if($params != null){
        $page = intval($params['page']);
        $count = intval($params['count']);
        $offset = ($page - 1) * $count;
        $limit = " LIMIT $offset, $count" ;
        $customerType = $params['customerType'];
        $condition = "";
        if($customerType == 1){
            $condition = "";
        }
        else if($customerType == 2){
            $condition .= " promoteStatus = 'Y'";
        }
        else if ($customerType == 3){
            $condition .= " promoteStatus = 'N' ";
        }
        else if ($customerType == 4){
            $condition .= " promoteStatus = 'N' AND date(current_timestamp) > date(Date_add(createTime,interval 14 day))";
        }
        if(!empty($params['search'])){
            $search = $params['search'];
            if($condition!="")
            $condition = $condition . "AND accountID = '$search' OR name LIKE '$search' ";
            else
            $condition = $condition . "accountID = '$search' OR name LIKE '$search' ";
          }
        if(!empty($params['storeID'])){
            $storeID = $params['storeID'];
            $storeID = implode(',',$storeID);
            if($condition!="")
            $condition .= "AND store in ($storeID)" ;
            else
            $condition .= "store in ($storeID)" ;
        }
        $selection = $this->db->query("SELECT * FROM registration WHERE $condition ORDER BY ID DESC $limit")->result_array();
        $selectionAmount = $this->db->query("SELECT * FROM registration WHERE $condition ORDER BY ID DESC")->result_array();
        $data['list'] = $selection;
        $data['total'] = sizeof($selectionAmount);
        return $data;
    }else{
        return $data;
    }
    }

    public function previewMessage( $params = null )
    {
        $query = "SELECT s1.name,s1.address,s1.telephone as phone,s2.text as district FROM registration as r
        LEFT JOIN store_final as s1 ON r.store = s1.ID 
        LEFT JOIN selection as s2 ON s2.type = 'district' AND s2.value = s1.parentKey
        ";
        $message = $params['message'];
        $pattern = $this->format($message);
        $customerType = $params['customerType'];
        $condition = "";
        if($customerType == 1){
            $condition = "";
        }
        else if($customerType == 2){
            $condition .= " WHERE r.promoteStatus = 'Y'";
        }
        else if ($customerType == 3){
            $condition .= " WHERE r.promoteStatus = 'N' ";
        }
        else if ($customerType == 4){
            $condition .= " WHERE r.promoteStatus = 'N' AND date(current_timestamp) > date(Date_add(r.createTime,interval 14 day))";
        }
        if(!empty($params['storeID'])){
            $storeID = $params['storeID'];
            $storeID = implode(',',$storeID);
            if($condition!="")
            $condition .= "AND store in ($storeID)" ;
            else
            $condition .= " WHERE store in ($storeID)" ;
        }
        $query = $query.$condition;
        $selection =  $this->db->query($query." LIMIT 1")->result_array();
        if(empty($selection) || $selection[0]['name'] == null){
            $this->error(422, "No record is matched");
            return;
        }
        else{
            $data['message'] = $this->reformMessage($pattern,$message,$selection[0]);
            return $data;
        }      
    }

    public function reconnectCustomer($params = null){
        $smsurl = "https://api.accessyou.com";
        $baseurl = "$smsurl/sms/sendsms-utf8-senderid.php?accountno=11033610&pwd=54483878&msg=";
        if($params == null){
            $this->error(422, "Invalid Input");
        }
        if($this->user['role'] != 'A'){
            $this->error(421, 'Permission Denied');
        }
        $query = "SELECT s1.name,s1.address,s1.telephone,s2.value as district,r.telephone FROM registration as r
        LEFT JOIN store_final as s1 ON r.store = s1.ID 
        LEFT JOIN selection as s2 ON s2.type = 'district' AND s2.value = s1.parentKey
        ";
        $message = $params['message'];
        $pattern = $this->format($message);
        $customerType = $params['customerType'];
        $condition = "";
        if($customerType == 1){
            $condition = "";
        }
        else if($customerType == 2){
            $condition .= " WHERE r.promoteStatus = 'Y'";
        }
        else if ($customerType == 3){
            $condition .= " WHERE r.promoteStatus = 'N' ";
        }
        else if ($customerType == 4){
            $condition .= " WHERE r.promoteStatus = 'N' AND date(current_timestamp) > date(Date_add(r.createTime,interval 14 day))";
        }
        if(!empty($params['storeID'])){
            $storeID = $params['storeID'];
            $storeID = implode(',',$storeID);
            if($condition!="")
            $condition .= "AND store in ($storeID)" ;
            else
            $condition .= " WHERE store in ($storeID)" ;
        }
        $query = $query.$condition;
        $selection =  $this->db->query($query)->result_array();
        if(empty($selection) || $selection[0]['name'] == null){
            $this->error(422, "No record is matched");
            return;
        }else{
            $this->db->trans_start();
            $batchCreate = array();
            $table_name = "sms";
            foreach ($selection as $key => $value) {
                $decodeMessage = $this->reformMessage($pattern,$message,$value);
                $encodedMessage = urlencode($decodeMessage);
                $telephone = $value["telephone"];
                $exacturl = "$baseurl$encodedMessage&phone=852$telephone&size=l&from=BauschLomb";
                $handle = fopen("$exacturl", "r");
                $param['message'] = $decodeMessage;
                $param['telephone'] = $value["telephone"];
                array_push($batchCreate,$param);
             }
             $this->db->insert_batch($table_name,$batchCreate);
             if ($this->db->trans_status() === false) {
                $this->db->trans_rollback();
                $this->error(400, $this->lang('error_create'));
            } else {
                $this->db->trans_commit();
            }
            return;  
        }
    }

    public function tailorMessage($params = null){
        $smsurl = "https://api.accessyou.com";
        $baseurl = "$smsurl/sms/sendsms-utf8-senderid.php?accountno=11033610&pwd=54483878&msg=";
        $message =  $params['message'];
        if(!empty($params['phone'])){
            $batchCreate = array();
            $table_name = 'sms';
            $this->db->trans_start();
            $phone = $params['phone'];
            foreach($phone as $value){
                $encodedMessage = urlencode($message);
                $telephone = $value;
                $exacturl = "$baseurl$encodedMessage&phone=852$telephone&size=l&from=BauschLomb";
                $handle = fopen("$exacturl", "r");
                $param['telephone'] = $value;
                $param['message'] = $message;
                array_push($batchCreate,$param);
            }
            $this->db->insert_batch($table_name,$batchCreate);
             if ($this->db->trans_status() === false) {
                $this->db->trans_rollback();
                $this->error(400, $this->lang('error_create'));
            } else {
                $this->db->trans_commit();
            }
        }
        else{
            $this->error(422,"Phone Number cannot be empty.");
        }
        return;
    }

    private function format($message) {
        // 1. regex
        // $message = 'Allow [[asd]], [[wer]]';
        preg_match_all("/{{(\s*.+?\s*)}}/",$message, $messageArray) ;
        return $messageArray;
        // 3. SQL Query
        // 4. dict result
        // 5. format SMS
        // 6. 
    }
    private function reformMessage($pattern,$message,$selection){
        $replaceArray = array();
            foreach ($pattern[1] as $key => $value) {
                switch($value){
                    case "name":{
                        array_push($replaceArray,$selection["name"]);
                    break;
                    }
                    case "address":{
                        array_push($replaceArray,$selection["address"]);
                    break;
                    }
                    case "phone":{
                        array_push($replaceArray,$selection["phone"]);
                    break;
                    }
                    case "district":{
                        array_push($replaceArray,$selection["district"]);
                    break;
                    }
                }
            }
            $newMessage = str_replace($pattern[0],$replaceArray,$message);
            return $newMessage;
    }


 }
