<?php
class Selection_model extends FIT_Model
{
    public function __construct(){
        parent::__construct(true);

    }
    public function getSelectionList()
    {
        $selection = $this->db->query('select * from selection WHERE version = "1"')->result_array();
        $data['list'] = $selection;
        $data['total'] = sizeof($selection);
        return $data;
    }

    public function getStoreList( $params = null )
    {
        $query = 'select s1.*, s2.text as district from store_final as s1
        LEFT JOIN selection as s2 ON s2.type = "district" AND s2.value = s1.parentKey WHERE s1.version = "1"
        ';
        $selection = $this->db->query($query)->result_array();
        $data['list'] = $selection;
        $data['total'] = sizeof($selection);

        return $data;       
    }


 }
