<?php
class Auth_model extends FIT_Model {

    public function __construct()
    {
        $this->load->database();

        // $this->load->model('user_model');

        // $ip = $_SERVER["REMOTE_ADDR"];

        // $query = "SELECT COUNT(*) AS count FROM `kol_user` WHERE IP = ? AND CREATE_TIME > NOW() - INTERVAL 1 DAY";
        // $count = $this->db->query($query, $ip)->row()->count;

        // if($count > 500)
        //     $this->error(400, 'error');

    }

    public function register($param)
    {
        // Check username and password empty or not
        if(empty($param['username']))
            $this->error(422, 'error_auth_username_empty');
        if(empty($param['role']))
            $this->error(422, 'error_auth_role_empty');
        // if(empty($param['password']))
        //     $this->error(422, $this->lang->line('error_auth_passward_empty'));        

        $user = [];
        $user['password'] = "BI". $param['username'];
        $user['username'] = $param['username'];
        $user['role'] = $param['role'];

        // $this->processParam($param['referralCode'], 'required', 'referralCode');

        // Check user existance
        $query = "SELECT 1 FROM user WHERE USERNAME = ?";
        $result = $this->db->query($query, $param['username'])->row_array();
        if($result != null)
            $this->error(422, 'error_auth_username_exist');        


        // $token = $this->input->get_request_header('Authorization', TRUE);

        // if($token != null){
        //     $query = "SELECT ID FROM kol_user WHERE TOKEN = ? AND USERNAME != '' ";
        //     $existingUser = $this->db->query($query, $token)->row_array();
        //     if($existingUser){
        //         $ID = $existingUser['ID'];
        //         $result = $this->customerToKol($param, $ID);
        //         return array('ID' => $result);
        //     }
        // }

        // Filter unnecessary fields
        // $user = $this->fieldsFilteredByEntity($param, 'user');

        // Encrypt password before insert into database
        
        // $user['shopLink'] = $this->createEncrypt(16);

        $this->db->trans_start();
        $user['salt'] = $this->createEncrypt(6);
        $user['password'] = md5($user['password'].$user['salt']);
        $user['token'] = $this->createEncrypt(32);

        $this->arrayKeysToSnakeCase($user);
        
        // Insert into user table
        $this->db->insert('user', $user);
        $userID = $this->db->insert_id();

        // $userDetail['userID'] = $userID;
        // $this->arrayKeysToSnakeCase($userDetail);
        // $this->db->insert('kol_user_detail', $userDetail);

        // $userIdentity['userID'] = $userID;
        // $this->arrayKeysToSnakeCase($userIdentity);
        // $this->db->insert('kol_user_identity', $userIdentity);    

        $this->commit('error_auth_register_failed');

        return array('ID' => $userID);
    }

    public function login($username, $password)
    {
        if(empty($username))
            $this->error(422, 'error_auth_username_empty');
        if(empty($password))
            $this->error(422, 'error_auth_passward_empty');  

        // Check user existance
        $query = "SELECT ID, PASSWORD, SALT, ROLE FROM user WHERE USERNAME = ?";
        $result = $this->db->query($query, $username)->row_array();

        if(empty($result))
            $this->error(422,'error_auth_username_notfound');

        // Validate password
        $_password = $result['PASSWORD'];
        $salt = $result['SALT'];
        $password = md5($password.$salt);

        if($_password != $password)
            $this->error(400, 'error_auth_passward_wrong');

        $user = array();
        $user['IP'] = $_SERVER["REMOTE_ADDR"];
        $user['LASTLOGIN'] = date('Y-m-d H:i:s');
        if($result['ROLE'] == "A"){
            $user['TOKEN'] = $this->createEncrypt(32);
        }
        
        $user['EXPIRETIME'] = date("Y-m-d H:i:s", strtotime('+90 days')); 

        $this->arrayKeysToSnakeCase($user);

        // Update login info
        $this->db->trans_start();
        $this->db->where('username',$username);
        $this->db->update('user', $user);

        $this->commit('error_auth_login_failed');

        $query = "SELECT ID, USERNAME, ROLE, TOKEN, EXPIRETIME, LASTLOGIN FROM user WHERE USERNAME = ?";
        $result = $this->db->query($query, $username)->row_array();

        return $result;

    }

    public function auth()
    {
        $token = $this->input->get_request_header('Authorization', TRUE);

        if(empty($token))    
            $this->error(401, 'error_unauthorized');

        // Select user info 
        $query = "SELECT ID, USERNAME, ROLE, TOKEN, EXPIRETIME FROM user WHERE TOKEN = ? ";
        $user = $this->db->query($query, $token)->row_array();

        // If user not exist
        if(empty($user))    
            $this->error(401,'error_unauthorized');

        // If token expired, need to login again
        $expireTime = $user['EXPIRETIME'];
        if($this->dateDifference($expireTime) > 0)
            $this->error(401,'error_unauthorized');
        else{
            $expireTime = date("Y-m-d H:i:s", strtotime('+90 day'));
        }

        $this->db->trans_start();
        $this->db->where('TOKEN',$token);
        $this->db->update('user', array('EXPIRETIME'=> $expireTime));

        $this->commit('error_auth_login_failed');

        $this->arrayKeysToCamelCase($user);

        return $user;
        
    }



// Merchant Management (User Management)


    public function multiRegister($param = null){
      // 09 Jan Update Merchant List 
        $merchant = [  
                // '502039',
                // '503999',
                // '500729',
                // '507369',
                // '506909',
                // '507239',
                // '505239',
                // '502239',
                // '504909',
                // '504809'
        ];

        foreach ($param as $key => $value) {
            $param = [];
            $param['username'] = $value;
            $param['role'] = "M";
            $result = $this->auth_model->register($param);
            return $result;                
        }
    }

    public function getMerchantList($param = null){
        $user = $this->user;
        if($user['role'] != 'A'){
            $this->error(421, 'Permission Denied');
        }
        $query = $this->db->query("SELECT * FROM user WHERE role = 'M' AND version = '1' ORDER BY username ASC ")->result_array();    
        $total = sizeof($query);  
        $data['list'] = $query;
        $data['total'] = $total;
        return $data;

    }
    






















    private function dateDifference($startDate, $endDate = null)
    {
        if(empty($endDate))
            $endDate = date("Y-m-d");
        return round((strtotime($endDate)-strtotime($startDate))/3600/24);
    }

    private function getRefererID($referralCode)
    {
        $this->processParam($referralCode, 'max-16|min-4', 'referralCode');

        if(empty($referralCode))
            return 0;

        $query = "
        SELECT USER_ID 
        FROM kol_user_detail 
        WHERE REFERRAL_CODE = ?
        ";

        $result = $this->db->query($query, $referralCode)->row_array();

        if(empty($result['USER_ID'])){
            $this->error(400, $this->lang('error_referal_code_invalid'));
        }

        return $result['USER_ID'];
    }

    public function sendCode($param)
    {
        $sms['sender'] = "KOLPartner";
        $sms['mobile'] = $this->processParam($param['mobile'], 'required|min-6|max-32', 'mobile');
        $sms['region'] = $this->processParam($param['region'], 'required|min-2|max-5', 'region');

        $verificationCode = $this->processParam($param['verificationCode'], 'required|length-6', 'verificationCode');

        $userID = $this->processParam($param['ID'], 'integer', 'ID');

        if(empty($userID)){
            $user = $this->auth();

            if(empty($user)){
                $this->error(401, $this->lang('error_unauthorized'));
            }
            $userID = $user['ID'];
        }

        if($sms['region'] == '+86'){
            $sms['message'] = "【网红热购】您的验证码是: $verificationCode";
        }else if($sms['region'] == '+852' || $sms['region'] == '+853'){
            $sms['message'] = "【KOL partner】您的驗證碼是: $verificationCode";
        }else{
            $sms['message'] = "【KOL partner】Your verification code is: $verificationCode";
        }

        $this->db->trans_start();
        $this->db->set('REGION', $sms['region']);
        $this->db->set('MOBILE', $sms['mobile']);
        $this->db->set('MOBILE_VERIFICATION_CODE', $verificationCode);
        $this->db->where('USER_ID', $userID);
        $this->update('kol_user_detail');

        $this->commit($this->lang('error_edit'));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, FIT_SERVICE_SMS_SEND);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization:'.FIT_SERVICE_TOKEN));    
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($sms));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);        

        if($result === false)
        {
            $this->error(422, curl_error($ch));
        }

        $result = json_decode($result);

        if($result->status != 200){
            $this->error(400, $this->lang('error_sms'));
        } 
    }

    public function verifyMobile($param)
    {
        $code = $this->processParam($param['verificationCode'], 'required|length-6', 'verificationCode');
        $userID = $this->processParam($param['ID'], 'integer', 'ID');

        if($code == "168168")
            return;

        if(empty($userID)){
            $user = $this->auth();

            if(empty($user)){
                $this->error(401, $this->lang('error_unauthorized'));
            }
            $userID = $user['ID'];
        }

        $query = "SELECT MOBILE_VERIFICATION_CODE FROM kol_user_detail WHERE USER_ID = ?";
        $result = $this->db->query($query, $userID)->row_array();

        $verificationCode = $result['MOBILE_VERIFICATION_CODE'];

        if($verificationCode != $code){
            $this->error('400', $this->lang('error_verification_code'));
        }

        $this->db->trans_start();
        $this->db->set('MOBILE_VERIFIED', 'Y');
        $this->db->where('USER_ID', $userID);
        $this->db->update('kol_user_detail');
        $this->commit($this->lang('error_edit'));
    }

    public function createToken()
    {
        $user = array();
        $user['role'] = 'C';
        $user['ip'] = $_SERVER["REMOTE_ADDR"];
        $user['token'] = $this->createEncrypt(32);
        $user['shopLink'] = $this->createEncrypt(16);
        $user['expireTime'] = date("Y-m-d H:i:s", strtotime('+100 year')); 

        $this->db->trans_start();

        $this->arrayKeysToSnakeCase($user);
        
        // Insert into user table
        $this->db->insert('kol_user', $user);
        $userID = $this->db->insert_id();

        $userDetail['userID'] = $userID;
        $this->arrayKeysToSnakeCase($userDetail);
        $this->db->insert('kol_user_detail', $userDetail);

        $userIdentity['userID'] = $userID;
        $this->arrayKeysToSnakeCase($userIdentity);
        $this->db->insert('kol_user_identity', $userIdentity);    

        $this->commit($this->lang('error_auth_register_failed'));

        return array("ID" => $userID, "token" => $user['TOKEN']);
    }

    private function customerToKol($param, $userID)
    {
        $username = $param['username'];
        $salt = $this->createEncrypt(6);
        $password = md5(md5($param['password']).$salt);
        $role = 'K';
        $expireTime = date("Y-m-d H:i:s", strtotime('+90 days')); 

        $this->db->trans_start();

        $this->db->set('USERNAME', $username);
        $this->db->set('SALT', $salt);
        $this->db->set('ROLE', $role);
        $this->db->set('PASSWORD', $password);
        $this->db->set('EXPIRE_TIME', $expireTime);
        $this->db->where('ID', $userID);
        $this->db->update('kol_user');

        $this->commit($this->lang('error_auth_register_failed'));

        return $userID;
    }

    public function generateReferer($from, $to){

        $param = array();

        if(empty($from) || empty($to))
            $this->error(400, 'error');

        for($i = $from; $i <= $to; $i++) {
            $param['username'] = 'super'.$i.'@kolpartner.com';
            $param['password'] = 'super'.$i;
            $param['referralCode'] = 'H001';
            $user = $this->register($param);
            $user['role'] = 'R';
            $user['referralCode'] = 'super'.$i;
            $this->user_model->setRole($user);
        }
        
    }

}