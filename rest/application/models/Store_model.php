<?php
class Store_model extends FIT_Model
{
    public function __construct(){
        parent::__construct('store');
      
    }

    // Registration Store List Module(A)

    public function createStore($params=null){
        if($params == null){
              $this->error(422, "Invalid Input");
        }
        if($this->user['role'] != 'A'){
            $this->error(421, 'Permission Denied');
        }
        
         $ID = $this->create($params);
          if(! $ID){
          $this->error(420, "Create Store Error");
          }

          $record =$this->db->query("SELECT * FROM store WHERE ID = $ID")->row_array();  

          if(empty($record)){
            $this->error(423, "Retrieve Store Error");
          }
          return $record;
    }

     public function updateStore($params=null){
        if($params == null){
              $this->error(422, "Invalid Input");
        }
        if($this->user['role'] != 'A'){
            $this->error(421, 'Permission Denied');
        }
        if(empty($params['ID'])){
              $this->error(422, "StoreID is missing");
        }
          $ID = $params['ID'];
          $record =$this->db->query(
            "UPDATE store SET 
            accountID = '".$params['accountID']."',
            name = '".$params['name']."',
            region = '".$params['region']."', 
            district = '".$params['district']."', 
            parentKey = '".$params['parentKey']."', 
            address = '".$params['address']."', 
            telephone = '".$params['telephone']."', 
            phone = '".$params['phone']."', 
            short = '".$params['short']."', 
            light = '".$params['light']."', 
            axis = '".$params['axis']."'
            WHERE ID = '".$ID."'");  
          if(!$record ){
            $this->error(423, "Update Store Error");
          }
          return $record;
    }

    public function getList($params = null ){

        $selection = $this->db->query('select * from store')->result_array();
        $data['list'] = $selection;
        $data['total'] = sizeof($selection);

      if($params != null){
        $page = intval($params['page']);
        $count = intval($params['count']);
        $offset = ($page - 1) * $count;
        $limit = " LIMIT $offset, $count" ;

        $condition = "";
        if(!empty($params['search'])){
            $search = $params['search'];
            $condition = $condition . "WHERE accountID = '$search' OR name LIKE '$search' ";
          }

        $selection = $this->db->query("SELECT * FROM store $condition ORDER BY ID DESC $limit")->result_array();
        $data['list'] = $selection;
        return $data;
    }else{
        return $data;
    }
    }

 public function delete($params=null){
        if($params == null){
              $this->error(422, "Invalid Input");
        }
        if($this->user['role'] != 'A'){
            $this->error(421, 'Permission Denied');
        }
        $ID = $params['ID'];
        $version = $params['version'];
         $delete = $this->db->query("UPDATE store SET version = '$version' WHERE ID = $ID");  
         return $delete;
    }

    public function export($params = null){
      if($params == null){
              $this->error(422, "Invalid Input");
        }
        $code = $this->processParam($params['code'], 'required');
        if( empty($code) || ($code != '2SF945XC!@SDF34') ) {
            $this->error(422, "Premission Denied");
        }
        $data = $this->db->query("SELECT * FROM `store` ORDER BY ID ASC")->result_array();
        return $data;
    }

// Static Store List Module(B) - For List View

    public function getStaticStoreList($params = null ){

        $selection = $this->db->query('SELECT * FROM store_static WHERE version = 1 ORDER BY region DESC, district ASC')->result_array();
        $data['list'] = $selection;
        $data['total'] = sizeof($selection);


      if($params != null){
        // For CMS API Parameter
        $page = intval($params['page']);
        $count = intval($params['count']);
        $offset = ($page - 1) * $count;
        $limit = " LIMIT $offset, $count" ;
        $condition = "version >= 0";

          if(!empty($params['search'])){
            $search = $params['search'];
            $condition = "name LIKE '$search' OR region LIKE '$search' OR district LIKE '$search' OR address LIKE '$search' OR telephone = '$search' ";
          }
      
        $selection = $this->db->query("SELECT * FROM store_static WHERE $condition ORDER BY region DESC, district ASC $limit")->result_array();

        $selectionAmount = $this->db->query("SELECT * FROM store_static WHERE $condition")->result_array();
        $data['list'] = $selection;
        $data['total'] = sizeof($selectionAmount);
        return $data;
    }else{
      // For Frontend API Parameter
        return $data;
    }
    }

    public function createStaticStore($params=null){
        if($params == null){
              $this->error(422, "Invalid Input");
        }
        if($this->user['role'] != 'A'){
            $this->error(421, 'Permission Denied');
        }
        $tableName = "store_static";
         $ID = $this->create($params,$tableName );
          if(! $ID){
          $this->error(420, "Create Store Error");
          }

          $record =$this->db->query("SELECT * FROM store_static WHERE ID = $ID")->row_array();  

          if(empty($record)){
            $this->error(423, "Retrieve Store Error");
          }
          return $record;
    }

     public function updateStaticStore($params=null){
        if($params == null){
              $this->error(422, "Invalid Input");
        }
        if($this->user['role'] != 'A'){
            $this->error(421, 'Permission Denied');
        }
        if(empty($params['ID'])){
              $this->error(422, "StoreID is missing");
        }
          $ID = $params['ID'];
          $record =$this->db->query(
            "UPDATE store_static SET 
            name = '".$params['name']."',
            region = '".$params['region']."', 
            district = '".$params['district']."', 
            address = '".$params['address']."', 
            telephone = '".$params['telephone']."', 
            latitude = '".$params['latitude']."',
            longitude = '".$params['longitude']."',
            product1 = '".$params['product1']."', 
            product2 = '".$params['product2']."', 
            product3 = '".$params['product3']."'
            WHERE ID = '".$ID."'");  
          if(!$record ){
            $this->error(423, "Update Store Error");
          }
          return $record;
    }

 

 public function deleteStaticStore($params=null){
        if($params == null){
              $this->error(422, "Invalid Input");
        }
        if($this->user['role'] != 'A'){
            $this->error(421, 'Permission Denied');
        }
        $ID = $params['ID'];
        $version = $params['version'];
         $delete = $this->db->query("UPDATE store_static SET version = '$version' WHERE ID = $ID");  
         return $delete;
    }

    public function exportStaticStore($params = null){
      if($params == null){
              $this->error(422, "Invalid Input");
        }
        $code = $this->processParam($params['code'], 'required');
        if( empty($code) || ($code != '2SF945XC!@SDF34') ) {
            $this->error(422, "Premission Denied");
        }
        $data = $this->db->query("SELECT * FROM `store_static` ORDER BY region DESC, district ASC ")->result_array();
        return $data;
    }
// Final Store List Module(B) - For List View

    public function getFinalStoreList($params = null ){

      $query = '
      SELECT s1.ID,s1.accountID,s1.name,s3.text as region,s2.text as district,s1.address,s1.telephone,s1.smsPhone,s1.short,s1.light,s1.axis,s1.latitude,s1.longitude,s3.text as region,s1.static_indicator as staticIndicator, s1.free_trial as freeTrial, s1.trial_offer as trialOffer FROM store_final as s1 
      LEFT JOIN selection as s2 ON s2.type = "district" AND s2.value = s1.parentKey
      LEFT JOIN selection as s3 ON s3.type = "region" AND s3.value = s2.parentKey
      ';
        $condition = "s1.version = 1";
        $order = "ORDER BY s1.ID DESC";
        $selection = $this->db->query($query."WHERE $condition $order")->result_array();
        $data['list'] = $selection;
        $data['total'] = sizeof($selection);
        // return $data;

      if($params != null){
        // For CMS API Parameter
        $page = intval($params['page']);
        $count = intval($params['count']);
        $offset = ($page - 1) * $count;
        $limit = " LIMIT $offset, $count" ;
        $condition = "s1.version >= 0";

          if(!empty($params['search'])){
            $search = $params['search'];
            $condition = "s1.name LIKE '%$search%' OR s3.text LIKE '$search' OR s2.text LIKE '$search' OR s1.address LIKE '$search' OR s1.telephone = '$search' ";
          }
          if(!empty($params['criteria'])){
            $search = $params['criteria'];
            $condition = "s1.name LIKE '%$search%' OR s1.ID LIKE '$search'";
          }
      
        $selection = $this->db->query(
        $query."WHERE $condition $order $limit
        ")->result_array();

        $selectionAmount = $this->db->query(
        $query." WHERE $condition $order
        ")->result_array();
        $data['list'] = $selection;
        $data['total'] = sizeof($selectionAmount);
        return $data;
    }else{
      // For Frontend API Parameter
        return $data;
    }
    }

    public function getFinalStoreDetail($params = null){
      $query = '
      SELECT s1.ID,s1.accountID,s1.name,s3.value as region,s2.value as district,s1.address,s1.telephone,s1.smsPhone,s1.short,s1.light,s1.axis,s1.latitude,s1.longitude,s1.static_indicator as staticIndicator, s1.free_trial as freeTrial, s1.trial_offer as trialOffer FROM store_final as s1 
      LEFT JOIN selection as s2 ON s2.type = "district" AND s2.value = s1.parentKey
      LEFT JOIN selection as s3 ON s3.type = "region" AND s3.value = s2.parentKey
      ';
      if(!empty($params['storeID'])){
        $ID = $params['storeID'];
        $condition = "s1.ID = '$ID'";
      }
      $selection = $this->db->query(
        $query." WHERE $condition
        ")->result();
        return $selection[0];
    }

    public function createFinalStore($params=null){
        if($params == null){
              $this->error(422, "Invalid Input");
        }
        if($this->user['role'] != 'A'){
            $this->error(421, 'Permission Denied');
        }
        $tableName = "store_final";
        $finalizedParams = $params;
        $finalizedParams["trial_offer"] = $params["trialOffer"];
        $finalizedParams["free_trial"] = $params["freeTrial"];
        $finalizedParams["type"] = "store";
        
        unset($finalizedParams["region"],$finalizedParams["district"],$finalizedParams["trialOffer"],$finalizedParams["freeTrial"]);

         $ID = $this->create($finalizedParams,$tableName );
          if(! $ID){
          $this->error(420, "Create Store Error");
          }

          $record =$this->db->query("SELECT * FROM store_final WHERE ID = $ID")->row_array();  

          if(empty($record)){
            $this->error(423, "Retrieve Store Error");
          }
          return $record;
    }

     public function updateFinalStore($params=null){
        if($params == null){
              $this->error(422, "Invalid Input");
        }
        if($this->user['role'] != 'A'){
            $this->error(421, 'Permission Denied');
        }
        if(empty($params['ID'])){
              $this->error(422, "StoreID is missing");
        }
          $ID = $params['ID'];
          $record =$this->db->query(
            "UPDATE store_final SET 
            name = '".$params['name']."', 
            address = '".$params['address']."',
            accountID = '".$params['accountID']."',
            parentKey = '".$params['parentKey']."', 
            telephone = '".$params['telephone']."', 
            smsPhone = '".$params['smsPhone']."', 
            latitude = '".$params['latitude']."',
            longitude = '".$params['longitude']."',
            short = '".$params['short']."', 
            light = '".$params['light']."', 
            axis = '".$params['axis']."',
            free_trial = '".$params['freeTrial']."',
            trial_offer = '".$params['trialOffer']."'
            WHERE ID = '".$ID."'");  
          if(!$record ){
            $this->error(423, "Update Store Error");
          }
          return $record;
    }

 

 public function deleteFinalStore($params=null){
        if($params == null){
              $this->error(422, "Invalid Input");
        }
        if($this->user['role'] != 'A'){
            $this->error(421, 'Permission Denied');
        }
        $ID = $params['ID'];
        $version = $params['version'];
         $delete = $this->db->query("UPDATE store_final SET version = '$version' WHERE ID = $ID");  
         return $delete;
    }

    public function exportFinalStore($params = null){
      if($params == null){
              $this->error(422, "Invalid Input");
        }
        $code = $this->processParam($params['code'], 'required');
        if( empty($code) || ($code != '2SF945XC!@SDF34') ) {
            $this->error(422, "Premission Denied");
        }
        $query = '
      SELECT s1.ID,s1.accountID,s1.name,s3.text as region,s2.text as district,s1.address,s1.telephone,s1.smsPhone,s1.short,s1.light,s1.axis,s1.latitude,s1.longitude,s1.version as region FROM store_final as s1 
      LEFT JOIN selection as s2 ON s2.type = "district" AND s2.value = s1.parentKey
      LEFT JOIN selection as s3 ON s3.type = "region" AND s3.value = s2.parentKey
      ';
        $data = $this->db->query($query)->result_array();
        return $data;
    }
   
 }
