<?php
class Redemption_model extends FIT_Model
{
    public function __construct(){
        parent::__construct('redemption');
      
    }


   public function getListHeader($type = 1){

       // $exportHeader = " 
       //   r.id as 'ID', r.telephone as '聯絡電話' , s1.text as '產品', s2.text as '現時佩戴產品', s3.text as '想試戴產品', s13.text as '接受推廣',r.store as '商店ID', s6.accountID as '商家編號',s4.text as '地區', s5.text as '地點', s6.name as '商店名稱', s6.address as '商店地址',r.createTime as '登記日期',r.promoteCode as '優惠碼', s16.text as '換領狀態',r.updateTime as '換領日期', r.version as '有效狀態' ";

       //  $listHeader = "
       //  r.id, r.telephone, r.region, r.district, r.store, r.product, r.previousProductType,
       //  r.expectedProductType, r.informationFlag, r.promoteCode, r.promoteStatus, r.createTime, r.updateTime, r.version ";

        //Redemption List

        $exportHeader = "r.ID as 'ID', r.promoteCode as '推廣優惠碼', r1.telephone as '客戶電話', r.sightProduct as '近視/遠視', r.lightProduct as '散光', r.axisProduct as '漸進', 
        concat('https://biotrue.tarvel.co/rest/', r.leftEyeImage) as '左眼圖片URL',  
        concat('https://biotrue.tarvel.co/rest/', r.rightEyeImage) as '右眼圖片URL', 
        r.leftLOT as '左眼LOT碼', r.rightLOT as '右眼LOT碼', r.createTime as '兌換時間', 
           r.redeemMerchant as '兌換商戶ID', 
           s2.name as '兌換店舖名稱', s3.text as '兌換店舖地區', s2.address as '兌換店舖地址', s2.telephone as '兌換店舖SMS電話',
           s1.name as '註冊店舖名稱', s4.text as '註冊店舖地區', s1.address as '註冊店舖地址', r.version";

        $listHeader = "r.ID, r.promoteCode, r.sightProduct, r.lightProduct, r.axisProduct, r.leftEyeImage, r.leftLOT, r.rightEyeImage, r.rightLOT, r.createTime, r.updateTime, r.redeemMerchant, r.version, r.redeemStoreID, 
            r1.telephone as clientTelephone, 
            s1.name as regStoreName, s4.text as regStoreDistrict, s1.address as regStoreAddress, s1.telephone as regStoreTelephone,
            s2.name as redeemStoreName, s3.text as redeemStoreDistrict, s2.address as redeemStoreAddress, s2.telephone as redeemStoreTelephone";

      if($type == 2){
        return $exportHeader;
      }else{
        return $listHeader;
      }
    }

    public function getListColumn($params=null){
     if(!empty($params['search'])){
        $search = $params['search'];
        $condition = "r.promoteCode LIKE '%$search%' 
                OR s2.telephone LIKE '%$search%' 
                OR r1.telephone LIKE '%$search%' 
                OR r.redeemMerchant LIKE '%$search%' 
                AND r.version = '1'
                ";
     }else{
        $condition = "r.version = '1' "; 
     }
      $listColumns = "  LEFT JOIN registration r1 ON r1.promoteCode = r.promoteCode
                        LEFT JOIN store s1 ON s1.ID = r1.store  
                        LEFT JOIN selection s4 ON s4.value = s1.parentKey AND s4.type = 'district'
                        LEFT JOIN store s2 ON s2.ID = r.redeemStoreID  
                        LEFT JOIN selection s3 ON s3.value = s2.parentKey AND s3.type = 'district' 
                        WHERE ". $condition;
      return $listColumns;
    }

    public function getList($params = null, $user = null)
    {
        // if($params != null ){
        //     $merchant = $params['merchant'];
        //     $query = $this->db->query("select * from redemption where merchant = '$merchant'")->result_array();    
        // }else{
        //     $query = $this->db->query('select * from redemption')->result_array();    
        // }

              // Get data base on Merchant ID
        // if(!empty($user)){
        //    // return $user;
        //    $versionFilter = $versionFilter . $this->redemption_model->getStoreCondictionByUser($user);
        // }

        $query = $this->db->query("SELECT * FROM redemption WHERE version = '1' ")->result_array();    
        $total = sizeof($query);  
        $header = $this->redemption_model->getListHeader(1);

        $columns = $this->redemption_model->getListColumn($params);

        if($params != null){
        $page = intval($params['page']);
        $count = intval($params['count']);
        $offset = ($page - 1) * $count;
        $limit = " LIMIT $offset, $count";

        $query = $this->db->query("SELECT $header FROM `redemption` r  $columns  ORDER BY ID  DESC $limit ")->result_array();   
    }
        $data['list'] = $query;
        $data['total'] = $total;
        return $data;
    }
  
  
    public function delete($params=null){
        if($params == null){
              $this->error(422, "Invalid Input");
        }
        $ID = $params['ID'];
         $delete = $this->db->query("UPDATE redemption SET version = '0' WHERE ID = $ID");  

         return $delete;
    }

  
    public function export($params = null){

        $header = $this->redemption_model->getListHeader(2);
        $columns = $this->redemption_model->getListColumn();
        $code = $this->processParam($params['code'], 'required');
        if( empty($code) || ($code != '2SF945XC!@SDF34') ) {
            $this->error(422, "Invalid Input");
        }
        $data = $this->db->query("SELECT $header FROM `redemption` r $columns ORDER BY ID DESC")->result_array();
        return $data;
    }

 }
