<?php

class Registration_model extends FIT_Model
{

  public function __construct(){
    parent::__construct('registration');


  }
  public function getListHeader($type = 1){
   $exportHeader = " 
   r.id as 'ID', r.telephone as '聯絡電話' , s1.text as '產品', s2.text as '現時佩戴產品', s3.text as '想試戴產品', s13.text as '接受推廣',r.store as '商店ID', s6.accountID as '商家編號',s4.text as '地區', s5.text as '地點', s6.name as '商店名稱', s6.address as '商店地址',r.createTime as '登記日期', r.phase as '登記版本', r.promoteCode as '優惠碼', s16.text as '換領狀態', r.updateTime as '換領日期', r.version as '有效狀態' ";

   $listHeader = "
   r.id, r.telephone, r.region, r.district, r.store, r.product, r.previousProductType,
   r.expectedProductType, r.informationFlag, r.promoteCode, r.promoteStatus, r.phase, r.createTime, r.updateTime, r.version";

   if($type == 2){
    return $exportHeader;
  }else{
    return $listHeader;
  }
}

public function getListColumn(){
  $listColumns = "  LEFT JOIN selection s1 ON s1.value = r.product and s1.type = 'product'
  LEFT JOIN selection s2 ON s2.value = r.previousProductType and s2.type = 'productType'
  LEFT JOIN selection s3 ON s3.value = r.expectedProductType and s3.type = 'productType'
  LEFT JOIN selection s4 ON s4.value = r.region  and s4.type = 'region'
  LEFT JOIN selection s5 ON s5.value = r.district and s5.parentKey = r.region 
  LEFT JOIN store s6 ON s6.ID = r.store and s6.parentKey = r.district 
  LEFT JOIN selection s13 ON s13.value = r.informationFlag and s13.type = 'flag'
  LEFT JOIN selection s16 ON s16.value = r.promoteStatus and s16.type = 'status'
  LEFT JOIN redemption r2 ON r2.promoteCode = r.promoteCode
  WHERE r.version = '1' ";
  return $listColumns;
}

public function getList($params = null, $user = null)
{

        // if($params != null ){
        //     $merchant = $params['merchant'];
        //     $query = $this->db->query("select * from registration where merchant = '$merchant'")->result_array();    
        // }else{
        //     $query = $this->db->query('select * from registration')->result_array();    
        // }

              // Get data base on Merchant ID
  $condition = " version = '1' ";
        // if(!empty($user)){
        //    // return $user;
        //    $versionFilter = $versionFilter . $this->registration_model->getStoreCondictionByUser($user);
        // }


  $header = $this->registration_model->getListHeader(1);


    // Pagination Handling
  if($params != null){
    $page = intval($params['page']);
    $count = intval($params['count']);
    $offset = ($page - 1) * $count;
    $limit = " LIMIT $offset, $count";

        // Search Param Handling
    if(!empty($params['search'])){
      $search = $params['search'];

        // MerchantID Search
      if(strlen($search) == 6){
        if(($search[0] == '5')&&($search[1] == '0')){
          $condition = $condition . "AND store IN (";
          $storeList = $this->registration_model->getStoreArrayByMerchantID($search);
          foreach ($storeList as $key => $value) {
            $storeID = $value['ID'];
            $condition = $condition . "'$storeID', ";
          }
          $condition = $condition . "'200')";
        }else{
          // promoteCode Search
         $condition = $condition . "AND promoteCode LIKE '%$search%' ";
       }

     }else{
      // Telephone Search
      $condition = $condition . " AND telephone LIKE '%$search%' ";
        }
      }

        // Phase Version Param Handling
      if(!empty($params['phase'])){
        $phase = $params['phase'];
          // if(($phase == 0)||($phase == '0')){
          //   return 
          // }

        $startDateTime = "'2019-12-01T00:00:00'";
        $version2ControllTimestamp = "'2020-04-01T14:00:00'";
        $version3ControllTimestamp = "'2020-07-01T00:00:00'";
        $endDateTime = "'2020-12-31T23:59:59'";

        if($phase == '3')
        {
            // New version 
            // $condition = $condition . "AND createTime >= " . $versionControllTimestamp;
          $condition = $condition . "AND createTime >= ". $version3ControllTimestamp ." AND createTime < ". $endDateTime ;
        }
        else if($phase == '2')
        {
            // $condition = $condition . "AND createTime < " . $versionControllTimestamp;
          $condition = $condition . "AND createTime >= ". $version2ControllTimestamp ." AND createTime < ". $version3ControllTimestamp ;

        }
        else
        {
            // $condition = $condition . "AND createTime < " . $versionControllTimestamp;
          $condition = $condition . "AND createTime >= ". $startDateTime ." AND createTime < ". $version2ControllTimestamp ;

        }

      }

      $query = $this->db->query("SELECT $header FROM `registration` r WHERE $condition ORDER BY ID DESC $limit ")->result_array();   
      $queryForTotal = $this->db->query("SELECT $header FROM `registration` r WHERE $condition ORDER BY ID DESC ")->result_array();       
      $total = sizeof($queryForTotal);  

    }
    $data['list'] = $query;
    $data['total'] = $total;
    
    return $data;
  }


  public function submit($params = null)
  {
    if($params == null){
      $this->error(422, "Invalid Input");
    }    
    $ID = $this->create($params);
    if(! $ID){
      $this.error(420, "Registration Generate Error");
    }
         // $data['ID'] =$ID ;
    $promoteCode = $this->getPromoteCode($ID);
    $currentPhase = $this->setCurrentPhase();
// Current Registratio Phase = 3
    $addPromoteCode = $this->db->query("UPDATE registration SET promoteCode = '$promoteCode' , phase ='$currentPhase', updateTime = NULL WHERE ID = $ID");  
          // $data['updatepromoteCode'] =$promoteCode ;
          // $data['result'] = $addPromoteCode ;

    if(! $addPromoteCode){
      $this.error(423, "PromoteCode Generate Error");
    }
    $record =$this->db->query("SELECT * FROM registration WHERE ID = $ID")->row_array();  

    return $record;

  }
    // backup
  public function getPromoteCode($ID){

      // Create Promote Code
         // $date = new DateTime();
         // $timeStamp = (string)$date->getTimestamp();
         // $timeIndex = rand(1001, 9999);
         // $code = 'BI'.$timeIndex.$ID;
   $lengthOfID = strlen((string)$ID);
   $lengthOfTimeindex = 6 - $lengthOfID;

   $minIndex = pow(10, ($lengthOfTimeindex - 1)) + 1; 
   $maxIndex =pow(10, $lengthOfTimeindex ) - 1; 

   $timeIndex = rand( $minIndex , $maxIndex );
   $code = "".$timeIndex.$ID;
   return $code;
 }


 public function isValid($params = null){
  if($params == null){
    $this->error(422, "Invalid Input");
  }
        // if(!empty($params['email'])){
        //     $var = 'email';
        // }
  if(!empty( $params['telephone'])){
    $var = 'telephone';
  }
  $sendQuery = "select r.*,s1.text as product from registration as r 
  LEFT JOIN selection as s1 ON s1.type = 'product' AND s1.value = r.product
  where r.$var = ? and r.version = '1'
  ";
$query = $this->db->query($sendQuery, array( $params[$var] ))->result_array();    // Fetch the Array if multi registration 

    $data['result'] = !empty($query); // Result, True: Invalid; False: Valid; A logic Bug here.
    if($data['result']){
      // Registered Telephone Promotion Status 
      $data['telephone'] = $params['telephone'];
      
      $existRegistration = $this->registrationStatusCheck($query);
      $currentPhase = $this->setCurrentPhase();
      
      $data['promoteStatus'] = $existRegistration['promoteStatus']; // If any record with a PromoteStatus = "Y"
      $data['maxPhase'] = $existRegistration['maxPhase'];  // Fetch 
      $data['product'] = $query[0]['product'];
      if( $data['promoteStatus'] == 'N'){
        // Current Version Registration Check
        if(intval($currentPhase) > intval($data['maxPhase'])){
          $data['result'] = false; // Override verification result. 
        }
      }

    }else{
      // Never registered telephone 
      return $data;
    }



    return $data; 
  }

  public function registrationStatusCheck($queryArray){
    $maxPhase = '0';
    $promoteStatus = 'N';
    foreach ($queryArray as $row) {
      // Check whether redeem
      if($row['promoteStatus'] == 'Y'){
        $promoteStatus = 'Y';
      }    

      if( intval($row['phase']) > intval($maxPhase) ){
        $maxPhase = $row['phase'];
      }

    }

    $existRegistration['maxPhase'] = $maxPhase;
    $existRegistration['promoteStatus'] = $promoteStatus;

    return $existRegistration;

  }

  public function delete($params=null){
    if($params == null){
      $this->error(422, "Invalid Input");
    }
    $ID = $params['ID'];
    $delete = $this->db->query("UPDATE registration SET version = '0' WHERE ID = $ID");  

  // Maybe need to delete redemption record also.

    return $delete;
  }

  public function emailService($params = null){
    $ID = $this->processParam($params['ID'], 'required');
    $subject = "Biotrue 買1送1優惠登記確認信";
    $content = "內容無法顯示，請諮詢客戶服務。";
    $query = $this->db->query("
     SELECT r.id, r.name, r.email, r.telephone, r.promoteCode,
     s4.text as 'region', s5.text as 'district', s6.name as 'store', s6.address as 'storeAddress', s6.telephone as 'storeTelephone'
     From registration r
     LEFT JOIN selection s4 on s4.value = r.region 
     LEFT JOIN selection s5 ON s5.value = r.district and s5.parentKey = r.region 
     LEFT JOIN store s6 ON s6.ID = r.store and s6.parentKey = r.district 
     WHERE r.id = $ID
     ")->row_array();   

    $content = $this->load->view('templates/emailTemplate',$query,true);
    $to = $query['email'];
    $emailService = $this->sendEmail($subject, $content, $to);
    $data['result'] = $emailService;
    return $data;
  }
  public function export($params = null){

    $header = $this->registration_model->getListHeader(2);
    $columns = $this->registration_model->getListColumn();
    $code = $this->processParam($params['code'], 'required');
    if( empty($code) || ($code != '2SF945XC!@SDF34') ) {
      $this->error(422, "Invalid Input");
    }

        // Phase Version Param Handling
    if(!empty($params['phase'])){
      $phase = $params['phase'];

      $startDateTime = "'2019-12-01T00:00:00'";
      $version2ControllTimestamp = "'2020-04-01T14:00:00'";
      $version3ControllTimestamp = "'2020-07-01T00:00:00'";
      $endDateTime = "'2020-12-31T23:59:59'";

      if($phase == '3')
      {
            // New version 
            // $condition = $condition . "AND createTime >= " . $versionControllTimestamp;
        $condition = "AND r.createTime >= ". $version3ControllTimestamp ." AND r.createTime < ". $endDateTime ;
      }
      else if($phase == '2')
      {
            // $condition = $condition . "AND createTime < " . $versionControllTimestamp;
        $condition = "AND r.createTime >= ". $version2ControllTimestamp ." AND r.createTime < ". $version3ControllTimestamp ;
      }
      else
      {
            // $condition = $condition . "AND createTime < " . $versionControllTimestamp;
        $condition = "AND r.createTime >= ". $startDateTime ." AND r.createTime < ". $version2ControllTimestamp ;
      }
    }
    $columns = $columns . $condition;

    $data = $this->db->query("SELECT $header FROM `registration` r $columns ORDER BY ID DESC")->result_array();
    return $data;
  }
  public function redeem($params=null, $user){
    if($params == null){
     $this->error(422, "Invalid Input");
   }    
   $promoteCode = $params['promoteCode'];

   $this->registration_model->promoteCodeVerify($promoteCode);

        $imageDir = "/var/www/html/biotrue_regional_web/rest/"; // both test server & Live Server

        $params['redeemMerchant']  =  $user['username'];

         // Convert Image files to url
        $image['left'] = $this->uploadImage('leftEyeImage', 1, $imageDir);
        $image['right'] = $this->uploadImage('rightEyeImage', 1, $imageDir);
        $params['leftEyeImage'] = $image['left'];
        $params['rightEyeImage'] =   $image['right'];

        $redemptionID = $this->create($params, 'redemption');
        if(!$redemptionID){
          $this->error(420, "無法創建兌換試戴優惠，請重新再試。");
        }
        $updatePromoteStatus = $this->db->query("UPDATE registration SET promoteStatus = 'Y' WHERE promoteCode = '$promoteCode'");  
        if(! $updatePromoteStatus){
          $this->error(423, "更新試戴優惠狀態錯誤，請稍候再試");
        }
        $record =$this->db->query("SELECT * FROM redemption WHERE ID = $redemptionID")->row_array();  
        return $record;
      }

      public function redeemLite($params = null){
       $promoteCode = $params['promoteCode'];
       $this->registration_model->promoteCodeVerify($promoteCode);

       $redemptionID = $this->create($params, 'redemption');
       if(!$redemptionID){
        $this->error(420, "無法創建兌換試戴優惠，請重新再試。");
      }
      $updatePromoteStatus = $this->db->query("UPDATE registration SET promoteStatus = 'Y' WHERE promoteCode = '$promoteCode'");  
      if(! $updatePromoteStatus){
        $this->error(423, "更新試戴優惠狀態錯誤，請稍候再試");
      }
      $record =$this->db->query("SELECT * FROM redemption WHERE ID = $redemptionID")->row_array();  
      return $record;
    }



    public function promoteCodeVerify($promoteCode){
            // verify PromoteCode
      $registrationRecord = $this->db->query("SELECT * FROM registration WHERE promoteCode = '$promoteCode' AND version = '1'")->row_array();  
      if(!$registrationRecord){
        $this->error(431, "該試戴優惠碼不正確，請重新兌換。");
      }

         // Verify Redemption
      $isPromotedCodeUsed = $this->db->query("SELECT * FROM redemption WHERE promoteCode = '$promoteCode' AND version = '1'")->row_array();  
      if(!empty($isPromotedCodeUsed)){
        $this->error(432, "該試戴優惠碼已兌換，請聯絡工作人員。");
      }
    }

    public function promoteCodeVerifyExpired($promoteCode){
      // verify PromoteCode
    $registrationRecord = $this->db->query("SELECT * FROM registration WHERE promoteCode = '$promoteCode' AND version = '1' AND date(current_timestamp) < date(Date_add(createTime,interval 14 day))")->row_array();  
    if($registrationRecord){
      $data['result'] = true;
    }
    else{
      $data['result'] = false;
    }
    return $data;
    }

    public function getStoreArrayByMerchantID($merchantID){

      $storeIDArray = $this->db->query("SELECT ID FROM `store_final` WHERE accountID = '$merchantID' ")->result_array();
        // if(!empty($storeIDArray)){
        //     foreach($storeIDArray as $value){
        //          $storeCondiction =  $storeCondiction . " OR " . "store = ". $value;
        //     }
        // }
      return $storeIDArray;
    }

    public function contactNumber($param = null ){
      if(empty($param['promoteCode'])){
       $this->error(422, "Invalid Input, promoteCode missing");
     }

     if(empty($param['storeID'])){
       $this->error(422, "Invalid Input, storeID missing");
     }

     $promoteCode = $param['promoteCode'];
     $storeID = $param['storeID'];

     $registrationRecord = $this->db->query("SELECT * FROM registration WHERE promoteCode = '$promoteCode' AND version = '1'")->row_array();  

     if(!$registrationRecord){
      $this->error(423, "查找用戶聯絡資料失敗，無法發送確認SMS");
    }
    $contact = [];
    $contact['client'] = $registrationRecord['telephone'];

         // Get the store telephone via store ID. 
    $storeRecord = $this->db->query("SELECT * FROM store WHERE ID = '$storeID'")->row_array();  
    if(!$storeRecord){
      $this->error(424, "查找商戶失敗，無法發送確認SMS");
    }
    $contact['merchant'] = $storeRecord['telephone'];
    return $contact;

  }
  public function updateDistrictParentkey($param = null ){
    if(empty($param)){
     $this->error(422, "Invalid Input, promoteCode missing");
   }

   $districtList = $this->db->query("SELECT * FROM selection WHERE type = 'district' and version = '1'")->result_array();  
   if(!$districtList){
    $this->error(424, "Fetch District List error");
  }

  foreach (range($param['startIndex'], $param['endIndex'] ) as $index) {

    $storeRecord = $this->db->query("SELECT * FROM store WHERE ID = '$index' and version = '1'")->row_array();  
    if(!$storeRecord){
      $this->error(425, "Get Store Record Failed". $index);
    }

    if( !empty( $storeRecord['parentKey'] )){
      continue;
    }

    $districtID = "";
    foreach ($districtList as $district) {
     if($district['text'] == $storeRecord['district']){
      $districtID = $district['value'];
    }
  }

  $storeUpdate = $this->db->query("UPDATE store SET parentKey = '$districtID' WHERE ID = '$index' AND version = '1' ");  
  if(!$storeUpdate){
    $this->error(425, "Update Record Failed". $index);
  }
}
}

public function setCurrentPhase($param = null){
  $currentTimeStamp = new DateTime('now');
  $version3ControllTimestamp = new DateTime('2020-07-01T00:00:00');
  if($currentTimeStamp >= $version3ControllTimestamp)
    return 3; 
  else
    return 2;

}

}
