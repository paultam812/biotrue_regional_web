<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</head>

<style type="text/css">

.primaryColor{
	color:#4AA148;

}

.textColor{
	color:#333;

}

.titleStyle{
	font-weight: 700;
	font-size: 18px;
}

.textStyle{
	font-weight: 600;
	font-size:14px;
}

.smallText{
	font-weight: 600;
	font-size:12px;
}

</style>


<body>
	<div style="background:#ffffff; max-width: 767px !important;">

	<!-- <div style="width: 100%; padding-top: 21.7%; background: url('https://upload.cc/i1/2018/10/18/OvpEXb.png'); background-repeat: no-repeat;background-size: contain; background-position: 50% 50%;">
	</div> -->
<!-- 	<div style="width: 100%; padding-top: 21.7%; background: url('https://test.tarvel.co/biotrue/image/email_header.png'); background-repeat: no-repeat;background-size: contain; background-position: 50% 50%;">
	</div>
 -->

 <img src="https://test.tarvel.co/biotrue/image/email_header.png" style="width: 100% !important; height: auto !important; display: block;">
	<div style="padding:15px;">
		<h5 style="color:#4AA148;font-weight: 700;font-size: 18px;">買1送1優惠確認信</h5>
		<h5 style="color:#333;font-weight: 600;font-size: 14px;margin-top: 20px;">多謝登記博士倫Biotrue<sup>®</sup> 1-DAY全舒適散光CON買1送1優惠。<br/>請查閱以下資料及於優惠有效期內到選定之商店出示此優惠確認信以享用優惠。</h5>
	
<div style="padding: 5px;">
<table style="width:100%">
  <tr>
    <td width="40%"><span style="padding-top:5px; color:#4AA148; font-weight: 600;font-size:14px;">優惠編號:</span></td>
    <td width="60%"><span style="padding-top:5px;	font-weight: 600;font-size:14px;"><?php echo $promoteCode ?></span></td> 
  </tr>
  <tr>
    <td width="40%"><span style="padding-top:5px; color:#4AA148; font-weight: 600;font-size:14px;">優惠詳情:</span></td>
    <td width="60%"><span style="padding-top:5px;	font-weight: 600;font-size:14px;">購買1盒博士倫Biotrue<sup>®</sup> 1-DAY全舒適散光CON, 即多送1盒</span></td> 
  </tr>
    <tr>
    <td width="40%"><span style="padding-top:5px; color:#4AA148; font-weight: 600;font-size:14px;">優惠有效期:</span></td>
    <td width="60%"><span style="padding-top:5px;	font-weight: 600;font-size:14px;">2018年11月2日至11月14日(逾期作廢)</span></td> 
  </tr>
    <tr>
    <td width="40%"><span style="padding-top:5px; color:#4AA148; font-weight: 600;font-size:14px;">姓名:</span></td>
    <td width="60%"><span style="padding-top:5px;	font-weight: 600;font-size:14px;"><?php echo $name ?></span></td> 
  </tr>
    <tr>
    <td width="40%"><span style="padding-top:5px; color:#4AA148; font-weight: 600;font-size:14px;">手提電話號碼:</span></td>
    <td width="60%"><span style="padding-top:5px;	font-weight: 600;font-size:14px;"><?php echo $telephone ?></span></td> 
  </tr>
  <tr>
    <td width="40%"><span style="padding-top:5px; color:#4AA148; font-weight: 600;font-size:14px;">選定之商店:</span></td>
    <td width="60%"><span style="padding-top:5px;	font-weight: 600;font-size:14px;"><?php echo $store ?></span></td> 
  </tr>
   <tr>
    <td width="40%"></td>
    <td width="60%"><span style="	font-weight: 600;font-size:14px;"><?php echo $storeAddress ?></span></td> 
  </tr>
   <tr>
    <td width="40%"></td>
    <td width="60%"><span style="font-weight: 600;font-size:14px;"><?php echo $storeTelephone ?></span></td> 
  </tr>
</table>
</div>


		<div style="margin-top: 30px;">
			<span style="color:#333;	font-weight: 600; font-size:12px;">注意:</span>
			<span style="color:#333;	font-weight: 600; font-size:12px;"><span style="color:#4AA148; text-decoration: underline;">請列印此確認信</span>並交給選定之商店以享用博士倫Biotrue<sup>®</sup> 1-DAY全舒適散光CON買1送1優惠。</span>
		</div>
		
		<div style="margin-top: 30px;">
			<h5 style="color:#333;	font-weight: 600; font-size:12px; margin-top: 5px; margin-bottom: 5px;">活動條款及細則:</h5>
			<h5 style="color:#333;	font-weight: 600; font-size:12px; margin-top: 5px; margin-bottom: 5px;">1. 每人只限使用一封優惠確認信，而每封優惠確認信只可使用一次。</h5>
			<h5 style="color:#333;	font-weight: 600; font-size:12px; margin-top: 5px; margin-bottom: 5px;">2. 須於優惠有效期內到選定之商店出示優惠確認信以享用博士倫Biotrue<sup>®</sup> 1-DAY全舒適散光CON買1送1優惠，逾期作廢。</h5>
			<h5 style="color:#333;	font-weight: 600; font-size:12px; margin-top: 5px; margin-bottom: 5px;">3. 使用優惠後，商店會撕去產品盒蓋以作記錄。</h5>
			<h5 style="color:#333;	font-weight: 600; font-size:12px; margin-top: 5px; margin-bottom: 5px;">4. 參加者必須經由駐場眼科視光師驗眼，並以視光師驗配之度數為準，駐場眼科視光師擁有最終決定權決定參加者是否適合佩戴是次推廣產品。如經眼科視光師驗眼後認為參加者不適合佩戴是次推廣產品，參加者將不可享用相關推廣優惠。</h5>
			<h5 style="color:#333;	font-weight: 600; font-size:12px; margin-top: 5px; margin-bottom: 5px;">5. 推廣優惠不可兌換現金或其他產品。</h5>
			<h5 style="color:#333;	font-weight: 600; font-size:12px; margin-top: 5px; margin-bottom: 5px;">6. 優惠不可轉讓。</h5>
			<h5 style="color:#333;	font-weight: 600; font-size:12px; margin-top: 5px; margin-bottom: 5px;">7. 如有任何爭議，博士倫(香港)有限公司擁有最終決定權。</h5>

		</div>


		<div style="text-align: center; margin-top: 40px; padding-left: 10px; padding-right: 10px;">
			<h5 style="color:#333;	font-weight: 600; font-size: 11px !important;">如有任何查詢，請於辦公時間內指點2213 3309或電郵至MKTG.BLHK@bausch.com ，我們會於辦公時間內回覆(辦公時間為星期一至五早上10時至12時，下午2時半至5時(公眾假期除外))。</h5>
			
		</div>

	</div>

	<div id="footer">

				<h5 style="color: #333; margin-bottom: 0px; font-size: 9px;">
					© 2018 Bausch & Lomb Incorporated. ®/™ are trademarks of Bausch & Lomb Incorporated or its affiliates. HK-VC-2018-09-041
				</h5>
			<!-- 	<div style="padding-top: 11.67%; background: url('../../image/email_footer.png'); background-repeat: no-repeat;background-size: contain; background-position: 50% 50%;">
				</div> -->

				 <img src="https://test.tarvel.co/biotrue/image/email_footer.png" style="width: 100% !important; height: auto !important; display: block;">

	</div>
</div>
</body>
</html>