<?php

class FIT_Model extends CI_Model
{

        // protected $validationRules = array();

    public function __construct( $tableName = "")
    {
        $this->tableName = $tableName;
        $db = $this->load->database();
        $this->load->model('util');
      $this->load->library(
            array(
                // 'form_validation',
                // 'util',
                'email_sender'
            )
        );
    }
 	public function create($item, $tableName = "")
    {
        // $this->validate();

        // $this->stripAttributes($item);

        // $item['version'] = 1;

        // $item['createUser'] = $uid;
        // $item['updateUser'] = $uid;
        
        // // $attachment = $this->processAttachments($item);
        // $this->arrayKeysToSnakeCase($item);
        // $this->checkFieldsExist($this->tableName, $item);



        $this->db->trans_start();
        if($tableName){
        $this->db->insert($tableName, $item);
        }else{
        $this->db->insert($this->tableName, $item);
    }
        $ID = $this->db->insert_id();

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            $this->error(400, $this->lang('error_create'));
        } else {
            $this->db->trans_commit();
        }

        return $ID;
    }
 	public function error($statusCode, $message)
    {
        // $validate = $this->input->get('validate');
        // if (!empty($validate)) {
        //     return;
        // }
        $this->util->error($statusCode, $message);
    }
    public function success($data = null, $message = 'OK')
    {
        $this->util->success($data, $this->lang->line('success'));
    }

    public function sendEmail($subject, $content, $to, $from = '')
    {
        return $this->email_sender->sendEmail($subject, $content, $to, $from = '');
    }

    public function loadSubject($type)
    {
        return $this->email_sender->loadSubject($type);
    }

    public function loadEmailTemplate($type, $param)
    {
        return $this->email_sender->loadEmailTemplate($type, $param);
    }

    public function processParam(&$value, $type = '', $fieldName = '')
    {
        $result = $value;

        $typeArray = explode('|', $type);

        // $validationRule['field'] = $fieldName;
        $rule = array(
            // "required": false,
            // "minLength": 1,
            // "maxLength": 99999999,
            // "minValue": 0,
            // "maxValue": 99999999,
            // "numeric": false,
            // "decimal": false,
            // "email": false,
            // "regex": ''
        );
        // $validationRule['rule'] = $rule;

        foreach ($typeArray as $type) {
            if ($type == 'page') {
                //If page not defined or page is not a number or page is less than 1, set page = 1
                $result = (empty($value) || !is_numeric($value) || $value < 1) ? 1 : $value;
                $rule['numeric'] = true;
            } elseif ($type == 'count') {
                //Limit number of records to 200 per page
                $result = (empty($value) || !is_numeric($value) || $value < 1) ? 10 : $value;
                // $result = $result > 200 ? 200 : $result;
                $rule['numeric'] = true;
            } elseif ($type == 'direction') {
                if (strtolower($value) != "asc" && strtolower($value) != "desc") {
                    $result = "";
                }
            } elseif ($type == 'ID') {
                //Parse ID array into string
                if (is_array($value)) {
                    foreach ($value as $v) {
                        if (!is_numeric($v)) {
                            $errorMessage = sprintf($this->lang('error_invalid_number'), $fieldName);
                            $this->error(422, $errorMessage);
                        }
                    }
                    $result = implode(',', $value);
                } else {
                    if (isset($value) && !is_numeric($value)) {
                        $errorMessage = sprintf($this->lang('error_invalid_number'), $fieldName);
                        $this->error(422, $errorMessage);
                    }
                }
            } elseif ($type == 'type') {
                if (is_array($value)) {
                    $result = "'${implode("','", $value)}'";
                } elseif (!empty($value)) {
                    $result = "'$value'";
                }
            } elseif ($type == 'date') {
                if (!strtotime($value) && !empty($value)) {
                    if (!empty($fieldName)) {
                        $errorMessage = sprintf($this->lang('error_invalid_date'), $fieldName);
                        $this->error(422, $errorMessage);
                    } else {
                        $this->error(422, $this->lang('error_invalid_input'));
                    }
                }
            } elseif ($type == 'number') {
                if (!is_numeric($value) && !empty($value)) {
                    if (!empty($fieldName)) {
                        $errorMessage = sprintf($this->lang('error_invalid_number'), $fieldName);
                        $this->error(422, $errorMessage);
                    } else {
                        $this->error(422, $this->lang('error_invalid_input'));
                    }
                }
                $rule['decimal'] = true;
            } elseif ($type == 'integer') {
                if (filter_var($value, FILTER_VALIDATE_INT) === false && !empty($value)) {
                    if (!empty($fieldName)) {
                        $errorMessage = sprintf($this->lang('error_invalid_integer'), $fieldName);
                        $this->error(422, $errorMessage);
                    } else {
                        $this->error(422, $this->lang('error_invalid_input'));
                    }
                }
                $rule['numeric'] = true;
            } elseif ($type == 'email') {
                if (filter_var($value, FILTER_VALIDATE_EMAIL) === false && !empty($value)) {
                    $this->error(422, $this->lang('error_invalid_email'));
                }
                $rule['email'] = true;
            } elseif ($type == 'required') {
                if (empty($value)) {
                    // if (!empty($fieldName)) {
                    //     $errorMessage = sprintf($this->lang('error_is_required'), $fieldName);
                    //     $this->error(422, $errorMessage);
                    // } else {
                        $this->error(422, "Required Input is missing");
                    // }
                }
                $rule['required'] = true;
            } elseif ($type == 'boolean') {
                if (empty($value)) {
                    $result = false;
                } else {
                    $result = ($value === 'true');
                }
            } elseif (substr($type, 0, 4) == 'max-') {
                $tmp = explode('-', $type);
                if (!isset($tmp[1])) {
                    continue;
                }

                $max = $tmp[1];

                if (in_array('number', $typeArray) || in_array('integer', $typeArray) || in_array('decimal', $typeArray)) {
                    $rule['maxValue'] = $max;
                } else {
                    $rule['maxLength'] = $max;
                }
            } elseif (substr($type, 0, 4) == 'min-') {
                $tmp = explode('-', $type);
                if (!isset($tmp[1])) {
                    continue;
                }

                $min = $tmp[1];

                if (in_array('number', $typeArray) || in_array('integer', $typeArray) || in_array('decimal', $typeArray)) {
                    $rule['minValue'] = $min;
                } else {
                    $rule['minLength'] = $min;
                }
            } else {
                $result = empty($value) ? '' : $value;
            }
        }
        // $validationRule['rule'] = $rule;

        // check duplication
        $duplicated = false;
        // foreach ($this->validationRules as $rule) {
        //     if ($fieldName == $rule['field']) {
        //         $duplicated = true;
        //     }
        // }

        // if (!$duplicated) {
        //     $this->validationRules[] = $validationRule;
        // }

        return $result;
    }
      public function createEncrypt($length = 6)
    {
        return $this->util->createEncrypt($length);
    }
  public function arrayKeysToSnakeCase(array &$array, $case = 'upper')
    {
        $this->util->arrayKeysToSnakeCase($array);
    }
       public function arrayKeysToCamelCase(array &$array)
    {
        $this->util->arrayKeysToCamelCase($array);
    }

       protected function commit($errorMsg)
    {
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            $this->error(400, $errorMsg);
        } else {
            $this->db->trans_commit();
        }
    }

    protected function uploadImage($key, $limit = 10, $host = '')
    {
        sleep(1);

        $url = array();


        $dir = 'uploadfile/'.date("Y").'/'.date("md").'/';
        $path = $host.$dir;

        if (!file_exists($path)){
            mkdir ($path,0777,true);
        }       
        $config["upload_path"] = $path;
        $config["allowed_types"] = 'gif|jpg|png|jpeg';
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        // echo json_encode($_FILES); die();

        if(isset($_FILES[$key])){

            $fileCount = count($_FILES[$key]['name']);
            $files = $_FILES[$key];

            if($fileCount > $limit)
                $this->error('422', 'error_too_much_files');

            if($fileCount == 0)
                return '';

            // Single file
            if(!is_array($files['name'])){
                $_FILES['file']['name']     = $files['name'];
                $_FILES['file']['type']     = $files["type"];
                $_FILES['file']['tmp_name'] = $files['tmp_name'];
                $_FILES['file']['error']    = $files['error'];
                $_FILES['file']['size']     = $files['size'];

                if($this->upload->do_upload('file'))
                {
                    $data = $this->upload->data();
                    $url = $dir.$data["file_name"];
                }else {
                    $this->error('422', 'Upload Image error');
                    // var_dump($this->upload->display_errors());
                }                
            }else{
                // Multiple files
                foreach ($files['name'] as $index => $fileName) {

                    $_FILES['file']['name']     = $fileName;
                    $_FILES['file']['type']     = $files["type"][$index];
                    $_FILES['file']['tmp_name'] = $files['tmp_name'][$index];
                    $_FILES['file']['error']    = $files['error'][$index];
                    $_FILES['file']['size']     = $files['size'][$index];

                    if($this->upload->do_upload('file'))
                    {
                        $data = $this->upload->data();
                        $url[] = $dir.$data["file_name"];
                    }else {
                        $this->error('422','error_file_upload');
                    }
                }
            }

        }else {

        }

        return $url;
    }
}

