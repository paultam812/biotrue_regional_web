<?php

defined('BASEPATH') or exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Authorization,Content-Type,responseType');

class FIT_Controller extends CI_Controller
{
    function __construct($noRedirect = false)
    {
        parent::__construct();
        $this->load->model('util');
        $this->load->library(
            array(
                // 'form_validation',
                // 'util',
                'email_sender'
            )
        );
        // Add to prevent AJAX Options call
        // if ($this->input->method(true) === 'OPTIONS') {
        //     return "ok";
        // }
    }

    public function checkHttpMethod($expectMethod)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != $expectMethod) {
            $this->error(405, $this->lang->line('error_bad_request'));
        }
    }
   // public function get()
   //  {
   //  }

   //  public function post()
   //  {
   //  }
      private function array2csv(array &$array)
    {
        if (count($array) == 0) {
            return null;
        }
        ob_start();
        $df = fopen("php://output", 'w');
        fwrite($df, chr(0xEF).chr(0xBB).chr(0xBF));
        fputcsv($df, array_keys(reset($array)));
        foreach ($array as &$row) {
            foreach ($row as &$str) {
                mb_convert_encoding($str, "UTF-8", "GBK");
            }

            fputcsv($df, $row);
        }
        fclose($df);
        return ob_get_clean();
    }

    private function download_send_headers($filename)
    {
        // disable caching
        $now = gmdate("D, d M Y H:i:s");
        header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");

        // force download
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header('Content-Type: text/csv;charset=utf-8');
        // disposition / encoding on response body
        header("Content-Disposition: attachment;filename={$filename}");
        header("Content-Transfer-Encoding: binary");
    }

    protected function exportCSV($fileName, $data)
    {
        $this->download_send_headers($fileName);
        echo $this->array2csv($data);
        die();
    }
   public function error($statusCode, $message)
    {
        $this->util->error($statusCode, $message);
    }

    public function success($data = null, $message = 'OK')
    {
        $this->util->success($data, $this->lang->line('success'));
    }

     public function createEncrypt($length = 6)
    {
        return $this->util->createEncrypt($length);
    }
    protected function exportExcel($fileName, $data)
    {
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();

        $row = 1;
        $column = 1;

        $headers = array_keys(reset($data));

        $char = 'A';
        foreach ($headers as $header) {
            // Check to make sure column index is in range
            $last = strlen($char) - 1;
            $byte = ord($char[$last]);
            if ($byte > 59 && $byte < 90) { // Within Range
                $char[$last] = chr($byte + 1);
            } elseif (strlen($char) > 1) {
            } else { // is last element in array and out of range
                for ($i = 0; $i < strlen($char); $i++) {
                    $char[$i] = 'A';
                }
                $char .= 'A';
            }
            // end of checking
            $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow($column, $row, $header);
            $spreadsheet->getActiveSheet()->getColumnDimension($char)->setAutoSize(true);
            $column = $column + 1;
        }

        $row = 2;
        $column = 1;

        foreach ($data as $line) {
            foreach ($line as $value) {
                $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow($column, $row, $value);
                $column = $column + 1;
            }
            $column = 1;
            $row = $row + 1;
        }

        
        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename=$fileName.xls");
        header('Cache-Control: max-age=0');
        $writer = IOFactory::createWriter($spreadsheet, "Xls");
        $writer->save("php://output");
    }

}