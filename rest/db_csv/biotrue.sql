-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Oct 09, 2018 at 07:17 AM
-- Server version: 5.6.35
-- PHP Version: 7.0.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `biotrue`
--
CREATE DATABASE IF NOT EXISTS `biotrue` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `biotrue`;

-- --------------------------------------------------------

--
-- Table structure for table `registration`
--

DROP TABLE IF EXISTS `registration`;
CREATE TABLE `registration` (
  `ID` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `gender` varchar(1) NOT NULL,
  `telephone` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `product` varchar(2) NOT NULL,
  `date` varchar(2) NOT NULL,
  `time` varchar(2) NOT NULL,
  `address` varchar(2) NOT NULL,
  `degree` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `selections`
--

DROP TABLE IF EXISTS `selections`;
CREATE TABLE `selections` (
  `ID` int(11) NOT NULL,
  `type` varchar(100) NOT NULL,
  `value` varchar(10) NOT NULL,
  `text` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `selections`
--

INSERT INTO `selections` (`ID`, `type`, `value`, `text`) VALUES
(1, 'address', '1', '8月18日（星期五）觀塘巧明街'),
(2, 'address', '2', '8月19日（星期六）銅鑼灣廣場1期'),
(3, 'address', '3', '8月20日（星期日）沙田正行街市'),
(4, 'address', '4', '8月25日（星期五）上環皇后大道近中環中心'),
(5, 'address', '5', '8月26日（星期六）旺角朗豪坊'),
(6, 'address', '6', '8月30日（星期三）尖沙咀海防道');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `registration`
--
ALTER TABLE `registration`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `selections`
--
ALTER TABLE `selections`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `registration`
--
ALTER TABLE `registration`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `selections`
--
ALTER TABLE `selections`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;